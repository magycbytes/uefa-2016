package com.magycbytes.euro2016.Screens.PlayersRank.helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.magycbytes.euro2016.Models.PlayerRanks.PlayersRanking;
import com.magycbytes.euro2016.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by alexandru on 6/15/16.
 */
public class PlayersRankAdapter extends RecyclerView.Adapter<PlayersRankAdapter.ViewHolder> {

    private final List<PlayersRanking> mPlayerRanks;
    private final Context mContext;

    public PlayersRankAdapter(List<PlayersRanking> playerRanks, Context context) {
        mPlayerRanks = playerRanks;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_player_rank,
                parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(mContext)
                .load(mPlayerRanks.get(position).getImage().getCardSmall())
                .into(holder.mPlayerImage);

        Picasso.with(mContext)
                .load(mPlayerRanks.get(position).getTeamLogo().getLogo70())
                .into(holder.mFlagImage);

        holder.mRankNumber.setText(String.valueOf(mPlayerRanks.get(position).getRank().getBarometerRank
                ()));
        holder.mPlayerName.setText(String.valueOf(mPlayerRanks.get(position).getWebNameAlt()));
    }

    @Override
    public int getItemCount() {
        return mPlayerRanks.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mPlayerImage;
        private final ImageView mFlagImage;
        private final TextView mPlayerName;
        private final TextView mRankNumber;

        public ViewHolder(View itemView) {
            super(itemView);

            mPlayerImage = (ImageView) itemView.findViewById(R.id.playerImage);
            mPlayerName = (TextView) itemView.findViewById(R.id.playerName);
            mRankNumber = (TextView) itemView.findViewById(R.id.rankNumber);
            mFlagImage = (ImageView) itemView.findViewById(R.id.countryFlag);
        }
    }
}
