package com.magycbytes.euro2016.Network;

import com.magycbytes.euro2016.Models.AllMatches.AllMatchesResponse;
import com.magycbytes.euro2016.Models.Groups.GroupResponse;
import com.magycbytes.euro2016.Models.TodayMatches.MatchesResponse;
import com.magycbytes.euro2016.Models.PlayerRanks.PlayerRankResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alexandru on 6/16/16.
 */
public interface FootballApiService {

    @GET("/api/v2/mobile/euro2016/{language}/groups")
    Call<GroupResponse> getGroups(@Path("language") String language, @Query("tournamentPhase") int phase);

    @GET("/api/v2/mobile/euro2016/{language}/playerhub")
    Call<PlayerRankResponse> getPlayersRank(@Path("language") String language, @Query("tournamentPhase") int phase);

    @GET("/api/v2/football/{language}/competitions/3/seasons/2016/matches/today?isMobileApp=true")
    Call<MatchesResponse> getTodayMatches(@Path("language") String language);

    @GET("/api/v2/mobile/euro2016/{language}/matches/matchday")
    Call<AllMatchesResponse> getAllMatches(@Path("language") String language, @Query
            ("tournamentPhase") int phase, @Query("$skip") int skipElements);
}
