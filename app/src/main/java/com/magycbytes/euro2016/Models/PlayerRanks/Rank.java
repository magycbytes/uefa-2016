package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.HashMap;
import java.util.Map;

public class Rank {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer playerBadge;
    private Double barometerPoints;
    private Integer barometerRank;
    private Integer noOfGames;
    private Integer barometerSlope;

    /**
     * @return The playerBadge
     */
    public Integer getPlayerBadge() {
        return playerBadge;
    }

    /**
     * @param playerBadge The playerBadge
     */
    public void setPlayerBadge(Integer playerBadge) {
        this.playerBadge = playerBadge;
    }

    /**
     * @return The barometerPoints
     */
    public Double getBarometerPoints() {
        return barometerPoints;
    }

    /**
     * @param barometerPoints The barometerPoints
     */
    public void setBarometerPoints(Double barometerPoints) {
        this.barometerPoints = barometerPoints;
    }

    /**
     * @return The barometerRank
     */
    public Integer getBarometerRank() {
        return barometerRank;
    }

    /**
     * @param barometerRank The barometerRank
     */
    public void setBarometerRank(Integer barometerRank) {
        this.barometerRank = barometerRank;
    }

    /**
     * @return The noOfGames
     */
    public Integer getNoOfGames() {
        return noOfGames;
    }

    /**
     * @param noOfGames The noOfGames
     */
    public void setNoOfGames(Integer noOfGames) {
        this.noOfGames = noOfGames;
    }

    /**
     * @return The barometerSlope
     */
    public Integer getBarometerSlope() {
        return barometerSlope;
    }

    /**
     * @param barometerSlope The barometerSlope
     */
    public void setBarometerSlope(Integer barometerSlope) {
        this.barometerSlope = barometerSlope;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
