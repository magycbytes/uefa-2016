package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Group {

    @SerializedName("idGroup")
    @Expose
    private Integer idGroup;
    @SerializedName("groupName")
    @Expose
    private String groupName;
    @SerializedName("groupLink")
    @Expose
    private String groupLink;
    @SerializedName("standings")
    @Expose
    private List<Standing> standings = new ArrayList<>();

    /**
     * @return The idGroup
     */
    public Integer getIdGroup() {
        return idGroup;
    }

    /**
     * @param idGroup The idGroup
     */
    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    /**
     * @return The groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName The groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return The groupLink
     */
    public String getGroupLink() {
        return groupLink;
    }

    /**
     * @param groupLink The groupLink
     */
    public void setGroupLink(String groupLink) {
        this.groupLink = groupLink;
    }

    /**
     * @return The standings
     */
    public List<Standing> getStandings() {
        return standings;
    }

    /**
     * @param standings The standings
     */
    public void setStandings(List<Standing> standings) {
        this.standings = standings;
    }

}
