package com.magycbytes.euro2016.Screens.AllMatches.Helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.euro2016.Models.AllMatches.MatchList;
import com.magycbytes.euro2016.R;

import java.util.List;

/**
 * Created by alexandru on 6/23/16.
 */

public class AllMatchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int STRING_TYPE = 1;
    private static final int DATA_TYPE = 2;

    private final List<Object> mObjects;

    public AllMatchesAdapter(List<Object> objects) {
        mObjects = objects;
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjects.get(position) instanceof String) {
            return STRING_TYPE;
        } else {
            return DATA_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        switch (viewType) {
            case STRING_TYPE:
                View view = LayoutInflater.from(context).inflate(R.layout.recycler_item_match_day, parent,
                        false);
                return new AllMatchesDayHolder(view);
            case DATA_TYPE:
                View matchView = LayoutInflater.from(context).inflate(R.layout
                        .recycler_item_match, parent, false);
                return new AllMatchesHolder(matchView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case STRING_TYPE:
                AllMatchesDayHolder dayHolder = (AllMatchesDayHolder) holder;
                dayHolder.setMatchDay((String) mObjects.get(position));
                break;
            case DATA_TYPE:
                AllMatchesHolder matchHolder = (AllMatchesHolder) holder;
                MatchList matchList = (MatchList) mObjects.get(position);
                matchHolder.showMatch(matchList);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    public int getDateItemsCount() {
        int count = 0;
        for (int i = 0; i < mObjects.size(); ++i) {
            if (getItemViewType(i) == DATA_TYPE) {
                count++;
            }
        }
        return count;
    }

    public List<Object> getObjects() {
        return mObjects;
    }
}
