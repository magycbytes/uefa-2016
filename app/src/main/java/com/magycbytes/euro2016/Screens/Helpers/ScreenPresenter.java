package com.magycbytes.euro2016.Screens.Helpers;

import android.content.Context;
import android.widget.Toast;

import com.magycbytes.euro2016.R;

/**
 * Created by alexandru on 6/23/16.
 */

public abstract class ScreenPresenter {

    protected final Context mContext;
    protected final ScreenView mScreenView;

    protected ScreenPresenter(Context context, ScreenView screenView) {
        mContext = context;
        mScreenView = screenView;
    }

    public void showErrorMessageToUser() {
        Toast.makeText(
                mContext,
                R.string.error_communicating_with_server,
                Toast.LENGTH_SHORT
        ).show();
    }

    public void showMessageToUser(int stringId) {
        Toast.makeText(
                mContext,
                mContext.getString(stringId),
                Toast.LENGTH_SHORT
        ).show();
    }

    public void hideProgress() {
        mScreenView.hideProgress();
    }

    protected void refresh() {
        mScreenView.showProgress();
    }
}
