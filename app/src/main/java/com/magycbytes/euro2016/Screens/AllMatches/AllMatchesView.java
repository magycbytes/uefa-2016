package com.magycbytes.euro2016.Screens.AllMatches;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.magycbytes.euro2016.Screens.AllMatches.Helpers.AllMatchesAdapter;
import com.magycbytes.euro2016.Screens.AllMatches.Helpers.RecyclerScrollListener;
import com.magycbytes.euro2016.Screens.Helpers.ScreenView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandru on 6/23/16.
 */

public class AllMatchesView extends ScreenView {


    private AllMatchesAdapter mAllMatchesAdapter;
    private RecyclerScrollListener mRecyclerScrollListener;

    AllMatchesView(View rootView) {
        super(rootView);

        setUpView();
    }

    public void addItems(List<Object> items) {
        List<Object> objects = mAllMatchesAdapter.getObjects();
        objects.addAll(items);
        mAllMatchesAdapter.notifyDataSetChanged();
    }

    public void clearItems() {
        mAllMatchesAdapter = new AllMatchesAdapter(new ArrayList<>());
        setAdapter(mAllMatchesAdapter);
    }

    @Override
    public void showProgress() {
        super.showProgress();
        mRecyclerScrollListener.setLoading(true);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        Log.i(this.toString(), "Set loading progress to false");
        mRecyclerScrollListener.setLoading(false);
    }

    @Override
    public int getItemsCountInList() {
        return mAllMatchesAdapter.getDateItemsCount();
    }

    void setEventsListener(RecyclerScrollListener.Events eventsListener) {
        mRecyclerScrollListener.setEventsListener(eventsListener);
    }

    void addScrollListener() {
        mMatchesList.addOnScrollListener(mRecyclerScrollListener);
    }

    void clearScrollListener() {
        mMatchesList.clearOnScrollListeners();
    }

    private void setUpView() {
        mAllMatchesAdapter = new AllMatchesAdapter(new ArrayList<>());
        setAdapter(mAllMatchesAdapter);

        mRecyclerScrollListener = new RecyclerScrollListener();
        mRecyclerScrollListener.setLayoutManager((LinearLayoutManager) mMatchesList.getLayoutManager());

        addScrollListener();
    }
}
