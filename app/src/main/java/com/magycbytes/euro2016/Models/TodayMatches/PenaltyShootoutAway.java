package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.HashMap;
import java.util.Map;


class PenaltyShootoutAway {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer idPlayer;
    private Integer minute;
    private Integer minuteExtra;
    private Integer phase;
    private Integer idEventCode;
    private String eventsubcode;
    private String playerWebName;
    private String playerWebNameAlt;
    private String eventDateCET;

    /**
     * @return The idPlayer
     */
    public Integer getIdPlayer() {
        return idPlayer;
    }

    /**
     * @param idPlayer The idPlayer
     */
    public void setIdPlayer(Integer idPlayer) {
        this.idPlayer = idPlayer;
    }

    /**
     * @return The minute
     */
    public Integer getMinute() {
        return minute;
    }

    /**
     * @param minute The minute
     */
    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    /**
     * @return The minuteExtra
     */
    public Integer getMinuteExtra() {
        return minuteExtra;
    }

    /**
     * @param minuteExtra The minuteExtra
     */
    public void setMinuteExtra(Integer minuteExtra) {
        this.minuteExtra = minuteExtra;
    }

    /**
     * @return The phase
     */
    public Integer getPhase() {
        return phase;
    }

    /**
     * @param phase The phase
     */
    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    /**
     * @return The idEventCode
     */
    public Integer getIdEventCode() {
        return idEventCode;
    }

    /**
     * @param idEventCode The idEventCode
     */
    public void setIdEventCode(Integer idEventCode) {
        this.idEventCode = idEventCode;
    }

    /**
     * @return The eventsubcode
     */
    public String getEventsubcode() {
        return eventsubcode;
    }

    /**
     * @param eventsubcode The eventsubcode
     */
    public void setEventsubcode(String eventsubcode) {
        this.eventsubcode = eventsubcode;
    }

    /**
     * @return The playerWebName
     */
    public String getPlayerWebName() {
        return playerWebName;
    }

    /**
     * @param playerWebName The playerWebName
     */
    public void setPlayerWebName(String playerWebName) {
        this.playerWebName = playerWebName;
    }

    /**
     * @return The playerWebNameAlt
     */
    public String getPlayerWebNameAlt() {
        return playerWebNameAlt;
    }

    /**
     * @param playerWebNameAlt The playerWebNameAlt
     */
    public void setPlayerWebNameAlt(String playerWebNameAlt) {
        this.playerWebNameAlt = playerWebNameAlt;
    }

    /**
     * @return The eventDateCET
     */
    public String getEventDateCET() {
        return eventDateCET;
    }

    /**
     * @param eventDateCET The eventDateCET
     */
    public void setEventDateCET(String eventDateCET) {
        this.eventDateCET = eventDateCET;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
