
package com.magycbytes.euro2016.Models.AllMatches;

import java.util.HashMap;
import java.util.Map;


public class LastComment {

    private String date;
    private String lastUpdateDate;
    private String datasource;
    private String resourceId;
    private String renderedContent;
    private Data data;
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The lastUpdateDate
     */
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * 
     * @param lastUpdateDate
     *     The lastUpdateDate
     */
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    /**
     * 
     * @return
     *     The datasource
     */
    public String getDatasource() {
        return datasource;
    }

    /**
     * 
     * @param datasource
     *     The datasource
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    /**
     * 
     * @return
     *     The resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * 
     * @param resourceId
     *     The resourceId
     */
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * 
     * @return
     *     The renderedContent
     */
    public String getRenderedContent() {
        return renderedContent;
    }

    /**
     * 
     * @param renderedContent
     *     The renderedContent
     */
    public void setRenderedContent(String renderedContent) {
        this.renderedContent = renderedContent;
    }

    /**
     * 
     * @return
     *     The data
     */
    public Data getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
