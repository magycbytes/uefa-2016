
package com.magycbytes.euro2016.Models.AllMatches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MatchInfoItem {

    private List<MatchList> matchList = new ArrayList<>();
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The matchList
     */
    public List<MatchList> getMatchList() {
        return matchList;
    }

    /**
     * 
     * @param matchList
     *     The matchList
     */
    public void setMatchList(List<MatchList> matchList) {
        this.matchList = matchList;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
