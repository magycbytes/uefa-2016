package com.magycbytes.euro2016.Screens.TodayMatches;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.euro2016.R;


public class MatchesFragment extends Fragment {

    private MatchesPresenter mMatchesPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matches_layout, container, false);

        MatchesView matchesView = new MatchesView(view);
        mMatchesPresenter = new MatchesPresenter(matchesView, getContext());
        mMatchesPresenter.startDownloading();

        return view;
    }

    public void refresh() {
        mMatchesPresenter.startDownloading();
    }

}
