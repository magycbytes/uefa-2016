package com.magycbytes.euro2016.Screens.AllMatches.Helpers;

import com.magycbytes.euro2016.Models.AllMatches.MatchList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alexandru on 6/23/16.
 */

class AllMatchesToAdapterConverter {

    private static final int MAX_MATCHES_FOR_DAY = 4;

    public List<Object> convert(
            HashMap<String, List<MatchList>> matches,
            List<String> keys,
            String lastDate) {

        List<Object> adapterData = new ArrayList<>(matches.size() * MAX_MATCHES_FOR_DAY);

        for (int j = 0; j < keys.size(); ++j) {
            String key = keys.get(j);
            List<MatchList> matchLists = matches.get(key);

            if (!key.equals(lastDate)) {
                adapterData.add(key);
            }
            for (int i = 0; i < matchLists.size(); ++i) {
                adapterData.add(matchLists.get(i));
            }

        }

        return adapterData;
    }
}
