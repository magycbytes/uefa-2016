package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.HashMap;
import java.util.Map;

class Player {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Rank rank;
    private Verbalization verbalization;
    private Integer idPlayer;
    private String webName;
    private String webNameAlt;
    private String name;
    private String surname;
    private String birthDate;
    private Integer idTeam;
    private String teamName;
    private String countryCode;
    private String countryName;
    private Integer fieldPos;
    private String fieldPosName;
    private Integer fieldSubPos;
    private String fieldSubPosName;
    private String fieldSubPosTag;
    private Image image;
    private TeamLogo teamLogo;

    /**
     * @return The rank
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * @param rank The rank
     */
    public void setRank(Rank rank) {
        this.rank = rank;
    }

    /**
     * @return The verbalization
     */
    public Verbalization getVerbalization() {
        return verbalization;
    }

    /**
     * @param verbalization The verbalization
     */
    public void setVerbalization(Verbalization verbalization) {
        this.verbalization = verbalization;
    }

    /**
     * @return The idPlayer
     */
    public Integer getIdPlayer() {
        return idPlayer;
    }

    /**
     * @param idPlayer The idPlayer
     */
    public void setIdPlayer(Integer idPlayer) {
        this.idPlayer = idPlayer;
    }

    /**
     * @return The webName
     */
    public String getWebName() {
        return webName;
    }

    /**
     * @param webName The webName
     */
    public void setWebName(String webName) {
        this.webName = webName;
    }

    /**
     * @return The webNameAlt
     */
    public String getWebNameAlt() {
        return webNameAlt;
    }

    /**
     * @param webNameAlt The webNameAlt
     */
    public void setWebNameAlt(String webNameAlt) {
        this.webNameAlt = webNameAlt;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname The surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return The birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate The birthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return The idTeam
     */
    public Integer getIdTeam() {
        return idTeam;
    }

    /**
     * @param idTeam The idTeam
     */
    public void setIdTeam(Integer idTeam) {
        this.idTeam = idTeam;
    }

    /**
     * @return The teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName The teamName
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return The countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * @param countryName The countryName
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * @return The fieldPos
     */
    public Integer getFieldPos() {
        return fieldPos;
    }

    /**
     * @param fieldPos The fieldPos
     */
    public void setFieldPos(Integer fieldPos) {
        this.fieldPos = fieldPos;
    }

    /**
     * @return The fieldPosName
     */
    public String getFieldPosName() {
        return fieldPosName;
    }

    /**
     * @param fieldPosName The fieldPosName
     */
    public void setFieldPosName(String fieldPosName) {
        this.fieldPosName = fieldPosName;
    }

    /**
     * @return The fieldSubPos
     */
    public Integer getFieldSubPos() {
        return fieldSubPos;
    }

    /**
     * @param fieldSubPos The fieldSubPos
     */
    public void setFieldSubPos(Integer fieldSubPos) {
        this.fieldSubPos = fieldSubPos;
    }

    /**
     * @return The fieldSubPosName
     */
    public String getFieldSubPosName() {
        return fieldSubPosName;
    }

    /**
     * @param fieldSubPosName The fieldSubPosName
     */
    public void setFieldSubPosName(String fieldSubPosName) {
        this.fieldSubPosName = fieldSubPosName;
    }

    /**
     * @return The fieldSubPosTag
     */
    public String getFieldSubPosTag() {
        return fieldSubPosTag;
    }

    /**
     * @param fieldSubPosTag The fieldSubPosTag
     */
    public void setFieldSubPosTag(String fieldSubPosTag) {
        this.fieldSubPosTag = fieldSubPosTag;
    }

    /**
     * @return The image
     */
    public Image getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * @return The teamLogo
     */
    public TeamLogo getTeamLogo() {
        return teamLogo;
    }

    /**
     * @param teamLogo The teamLogo
     */
    public void setTeamLogo(TeamLogo teamLogo) {
        this.teamLogo = teamLogo;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
