package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.HashMap;
import java.util.Map;

public class Verbalization {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer bestStatValue;
    private String bestStatName;
    private String phrase;
    private String verbalizationTag;

    /**
     * @return The bestStatValue
     */
    public Integer getBestStatValue() {
        return bestStatValue;
    }

    /**
     * @param bestStatValue The bestStatValue
     */
    public void setBestStatValue(Integer bestStatValue) {
        this.bestStatValue = bestStatValue;
    }

    /**
     * @return The bestStatName
     */
    public String getBestStatName() {
        return bestStatName;
    }

    /**
     * @param bestStatName The bestStatName
     */
    public void setBestStatName(String bestStatName) {
        this.bestStatName = bestStatName;
    }

    /**
     * @return The phrase
     */
    public String getPhrase() {
        return phrase;
    }

    /**
     * @param phrase The phrase
     */
    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    /**
     * @return The verbalizationTag
     */
    public String getVerbalizationTag() {
        return verbalizationTag;
    }

    /**
     * @param verbalizationTag The verbalizationTag
     */
    public void setVerbalizationTag(String verbalizationTag) {
        this.verbalizationTag = verbalizationTag;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
