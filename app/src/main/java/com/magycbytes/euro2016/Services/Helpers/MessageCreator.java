package com.magycbytes.euro2016.Services.Helpers;

import android.content.Context;

import com.magycbytes.euro2016.R;

import java.util.concurrent.TimeUnit;

/**
 * Created by alexandru on 6/18/16.
 */
public class MessageCreator {

    private static final String REMAINING_MINUTES_FORMAT = "%s %s %s";

    public static String getMessage(Context context, long milliseconds) {
        long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);
        if (hours < 1) {
            long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);
            if (minutes < 1) {
                return context.getString(R.string.right_now);
            } else {
                return String.format(REMAINING_MINUTES_FORMAT, context.getString(R.string.in), String.valueOf
                        (minutes), context.getString(R.string.minutes));
            }
        } else {
            return context.getString(R.string.in_one_hour);
        }
    }
}
