package com.magycbytes.euro2016;

import android.content.Context;
import android.view.View;
import android.widget.Spinner;

import com.magycbytes.euro2016.AppSettings.UserPreferences;

/**
 * Created by alexandru on 6/15/16.
 */
class StandingsLayoutSelectorView {

    private final Spinner mSpinner;

    public StandingsLayoutSelectorView(View view) {
        Context context = view.getContext();

        mSpinner = (Spinner) view.findViewById(R.id.spinner);
        if (mSpinner != null) {
            String[] spinnerItems = {
                    context.getString(R.string.grid),
                    context.getString(R.string.list)
            };
            mSpinner.setAdapter(new ViewOptionsAdapter(context, spinnerItems, R.layout.spinner_item));
        }

        assert mSpinner != null;
        mSpinner.setSelection(UserPreferences.getViewPreference(context));
        mSpinner.setOnItemSelectedListener(new StandingsSelectorListener());
    }

    public void showSpinner() {
        mSpinner.setVisibility(View.VISIBLE);
    }

    public void hideSpinner() {
        mSpinner.setVisibility(View.GONE);
    }
}
