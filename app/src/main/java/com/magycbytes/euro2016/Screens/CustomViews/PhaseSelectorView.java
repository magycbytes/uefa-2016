package com.magycbytes.euro2016.Screens.CustomViews;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.ViewOptionsAdapter;

/**
 * Created by alexandru on 6/16/16.
 */
public class PhaseSelectorView {

    private final Spinner mSpinner;

    public PhaseSelectorView(View rootView, int currentItemSelected) {
        mSpinner = (Spinner) rootView.findViewById(R.id.tournamentPhase);

        mSpinner.setAdapter(new ViewOptionsAdapter(rootView.getContext(), rootView.getContext()
                .getResources().getStringArray(R.array.tournament_phases), R.layout.spinner_item_black_text));
        mSpinner.setSelection(currentItemSelected);
    }

    public void setOnItemSelected(AdapterView.OnItemSelectedListener listener) {
        mSpinner.setOnItemSelectedListener(listener);
    }
}
