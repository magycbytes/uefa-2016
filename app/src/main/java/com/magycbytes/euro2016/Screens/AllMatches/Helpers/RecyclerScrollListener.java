package com.magycbytes.euro2016.Screens.AllMatches.Helpers;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

/**
 * Created by alexandru on 6/24/16.
 */

public class RecyclerScrollListener extends RecyclerView.OnScrollListener {

    private boolean mIsLoading = false;
    private LinearLayoutManager mLayoutManager;
    private Events mEventsListener;

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy < 1 || mIsLoading || mLayoutManager == null) {
            return;
        }

        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= PAGE_SIZE) {
            if (mEventsListener != null) {
                mEventsListener.loadMoreItems();
            }
        }
    }

    public void setLoading(boolean loading) {
        mIsLoading = loading;
    }

    public void setEventsListener(Events eventsListener) {
        mEventsListener = eventsListener;
    }

    public void setLayoutManager(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    public interface Events {
        void loadMoreItems();
    }
}
