package com.magycbytes.euro2016.Services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.magycbytes.euro2016.R;

import java.util.Random;

/**
 * Created by alexandru on 6/18/16.
 */
public class TimeToMatchNotificationService extends IntentService {

    public static final int ID = 1;
    private static final String MESSAGE_TO_SHOW = "messageToShow";
    private static final String TITLE_TO_SHOW = "titleToShow";

    public TimeToMatchNotificationService() {
        super("TimeToMatchService"); //NON-NLS
    }

    public static Intent getIntent(Context context, String title, String message) {
        Intent intent = new Intent(context, TimeToMatchNotificationService.class);
        intent.putExtra(TITLE_TO_SHOW, title);
        intent.putExtra(MESSAGE_TO_SHOW, message);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(this.toString(), "In showing notification");


        String message = intent.getStringExtra(MESSAGE_TO_SHOW);
        String title = intent.getStringExtra(TITLE_TO_SHOW);

        Log.i(this.toString(), title + ", " + message);

        int id = new Random().nextInt();
        Log.i(this.toString(), "With id: " + id);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setContentText(message)
                        .setSmallIcon(R.drawable.ic_match_notification_white_24dp)
                        .setContentTitle(title)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true)
                        .setLights(Color.BLUE, 500, 500);

        NotificationManager notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, builder.build());
    }
}
