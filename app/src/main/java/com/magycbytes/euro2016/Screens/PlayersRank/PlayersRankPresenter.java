package com.magycbytes.euro2016.Screens.PlayersRank;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.Toast;

import com.magycbytes.euro2016.AppSettings.UserInterface;
import com.magycbytes.euro2016.Models.PlayerRanks.PlayerRankResponse;
import com.magycbytes.euro2016.Models.PlayerRanks.PlayersRanking;
import com.magycbytes.euro2016.Network.Callbacks.PlayerRanksCallback;
import com.magycbytes.euro2016.Network.ManagerApi;
import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Screens.PlayersRank.helpers.PlayersRankAdapter;

import java.util.List;

import retrofit2.Call;

/**
 * Created by alexandru on 6/15/16.
 */
public class PlayersRankPresenter implements SwipeRefreshLayout.OnRefreshListener {

    private final PlayersRankView mRankView;
    private final Context mContext;

    public PlayersRankPresenter(PlayersRankView rankView, Context context) {
        mRankView = rankView;
        mContext = context;

        mRankView.setRefreshListener(this);
    }

    public void hideProgress() {
        mRankView.hideProgress();
    }

    public void showPlayers(List<PlayersRanking> playerRanks) {
        PlayersRankAdapter adapter = new PlayersRankAdapter(playerRanks, mContext);
        mRankView.setAdapter(adapter);
    }

    public void showErrorMessageToUser() {
        Toast.makeText(mContext, R.string.error_communicating_with_server, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onRefresh() {
        mRankView.showProgress();
        Call<PlayerRankResponse> call = ManagerApi.getApi(mContext).getPlayersRank(UserInterface
                .getInterfaceLanguage(mContext), 2);
        call.enqueue(new PlayerRanksCallback(this));
    }
}
