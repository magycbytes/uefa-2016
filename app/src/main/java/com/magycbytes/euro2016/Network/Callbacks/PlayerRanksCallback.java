package com.magycbytes.euro2016.Network.Callbacks;

import android.util.Log;

import com.magycbytes.euro2016.Models.PlayerRanks.PlayerRankResponse;
import com.magycbytes.euro2016.Screens.PlayersRank.PlayersRankPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandru on 6/21/16.
 */

public class PlayerRanksCallback implements Callback<PlayerRankResponse> {

    private final PlayersRankPresenter mPresenter;

    public PlayerRanksCallback(PlayersRankPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResponse(Call<PlayerRankResponse> call, Response<PlayerRankResponse> response) {
        mPresenter.hideProgress();

        if (response.code() != 200 || response.body() == null) {
            mPresenter.showErrorMessageToUser();
            return;
        }

        mPresenter.showPlayers(response.body().getPlayersRanking());
    }

    @Override
    public void onFailure(Call<PlayerRankResponse> call, Throwable t) {
        mPresenter.hideProgress();
        if (t == null || t.getMessage() == null) {
            return;
        }
        Log.e(this.toString(), t.getMessage());
        mPresenter.showErrorMessageToUser();
    }
}
