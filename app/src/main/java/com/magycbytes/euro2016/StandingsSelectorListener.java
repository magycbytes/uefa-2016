package com.magycbytes.euro2016;

import android.view.View;
import android.widget.AdapterView;

import com.magycbytes.euro2016.AppSettings.UserPreferences;

/**
 * Created by alexandru on 6/21/16.
 */

class StandingsSelectorListener implements AdapterView.OnItemSelectedListener {
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (view == null) {
            return;
        }
        UserPreferences.saveViewPreference(view.getContext(), i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
