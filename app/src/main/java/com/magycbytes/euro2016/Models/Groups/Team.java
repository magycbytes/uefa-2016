package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Team {

    @SerializedName("idTeam")
    @Expose
    private Integer idTeam;
    @SerializedName("webName")
    @Expose
    private String webName;
    @SerializedName("officialName")
    @Expose
    private String officialName;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("typeIsNational")
    @Expose
    private Boolean typeIsNational;
    @SerializedName("teamCode")
    @Expose
    private String teamCode;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("teamLogo")
    @Expose
    private TeamLogo teamLogo;

    /**
     * @return The idTeam
     */
    public Integer getIdTeam() {
        return idTeam;
    }

    /**
     * @param idTeam The idTeam
     */
    public void setIdTeam(Integer idTeam) {
        this.idTeam = idTeam;
    }

    /**
     * @return The webName
     */
    public String getWebName() {
        return webName;
    }

    /**
     * @param webName The webName
     */
    public void setWebName(String webName) {
        this.webName = webName;
    }

    /**
     * @return The officialName
     */
    public String getOfficialName() {
        return officialName;
    }

    /**
     * @param officialName The officialName
     */
    public void setOfficialName(String officialName) {
        this.officialName = officialName;
    }

    /**
     * @return The shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName The shortName
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return The typeIsNational
     */
    public Boolean getTypeIsNational() {
        return typeIsNational;
    }

    /**
     * @param typeIsNational The typeIsNational
     */
    public void setTypeIsNational(Boolean typeIsNational) {
        this.typeIsNational = typeIsNational;
    }

    /**
     * @return The teamCode
     */
    public String getTeamCode() {
        return teamCode;
    }

    /**
     * @param teamCode The teamCode
     */
    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return The teamLogo
     */
    public TeamLogo getTeamLogo() {
        return teamLogo;
    }

    /**
     * @param teamLogo The teamLogo
     */
    public void setTeamLogo(TeamLogo teamLogo) {
        this.teamLogo = teamLogo;
    }

}
