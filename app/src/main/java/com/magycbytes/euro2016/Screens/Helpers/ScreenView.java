package com.magycbytes.euro2016.Screens.Helpers;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.magycbytes.euro2016.R;

/**
 * Created by alexandru on 6/23/16.
 */

public abstract class ScreenView {

    protected final RecyclerView mMatchesList;
    private final SwipeRefreshLayout mRefreshLayout;

    protected ScreenView(View rootView) {
        mMatchesList = (RecyclerView) rootView.findViewById(R.id.data_list);
        mRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);


        mMatchesList.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        mRefreshLayout.setColorSchemeResources(
                R.color.colorAccent,
                R.color.colorPrimaryDark,
                android.R.color.white
        );
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        mMatchesList.setAdapter(adapter);
    }

    public void showProgress() {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        });
    }

    public void hideProgress() {
        mRefreshLayout.setRefreshing(false);
    }

    public void setRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        mRefreshLayout.setOnRefreshListener(listener);
    }

    public int getItemsCountInList() {
        return mMatchesList.getAdapter().getItemCount();
    }

    protected void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        mMatchesList.setLayoutManager(layoutManager);
    }
}
