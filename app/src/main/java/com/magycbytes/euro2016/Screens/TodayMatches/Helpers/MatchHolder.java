package com.magycbytes.euro2016.Screens.TodayMatches.Helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.magycbytes.euro2016.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by alexandru on 6/17/16.
 */
public class MatchHolder extends RecyclerView.ViewHolder{

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss ZZZZZ";
    private static final String TIME_FORMAT = "HH:mm";

    protected final Context mContext;

    private final ImageView mHomeTeamFlag;
    private final ImageView mAwayTeamFlag;
    private final TextView mHomeTeamGoals;
    private final TextView mAwayTeamGoals;
    private final TextView mStartHour;
    private final TextView mHomeTeamName;
    private final TextView mAwayTeamName;
    private final TextView mGroupName;
    private final TextView mCurrentMinute;
    private final RelativeLayout mGroupNameLayout;

    public MatchHolder(View itemView) {
        super(itemView);

        mContext = itemView.getContext();

        mHomeTeamFlag = (ImageView) itemView.findViewById(R.id.homeTeamFlag);
        mAwayTeamFlag = (ImageView) itemView.findViewById(R.id.awayTeamFlag);
        mHomeTeamGoals = (TextView) itemView.findViewById(R.id.homeTeamGoals);
        mAwayTeamGoals = (TextView) itemView.findViewById(R.id.awayTeamGoals);
        mStartHour = (TextView) itemView.findViewById(R.id.startHour);
        mHomeTeamName = (TextView) itemView.findViewById(R.id.homeTeamName);
        mAwayTeamName = (TextView) itemView.findViewById(R.id.awayTeamName);
        mGroupName = (TextView) itemView.findViewById(R.id.groupName);
        mCurrentMinute = (TextView) itemView.findViewById(R.id.currentMinute);
        mGroupNameLayout = (RelativeLayout) itemView.findViewById(R.id.groupNameLayout);
    }

    private void showStartHour(String dateTime) {
        String hour = "?";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.getDefault());
        try {
            Date date = simpleDateFormat.parse(dateTime);
            SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
            hour = timeFormat.format(date);
        } catch (ParseException e) {
            Log.e(this.toString(), e.getMessage());
        }
        mStartHour.setText(hour);

        mHomeTeamGoals.setVisibility(View.GONE);
        mAwayTeamGoals.setVisibility(View.GONE);
    }

    protected void loadFlags(Context context, String homeUrl, String awayUrl) {
        Picasso.with(context)
                .load(homeUrl)
                .into(mHomeTeamFlag);

        Picasso.with(context)
                .load(awayUrl)
                .into(mAwayTeamFlag);
    }

    public void showGoals(Integer homeTeam, Integer awayTeam, String startDate) {
        if (homeTeam != null) {
            mHomeTeamGoals.setVisibility(View.VISIBLE);
            mAwayTeamGoals.setVisibility(View.VISIBLE);
            mStartHour.setText(R.string.unknown_data);
            mHomeTeamGoals.setText(String.valueOf(homeTeam));
            mAwayTeamGoals.setText(String.valueOf(awayTeam));
        } else {
            showStartHour(startDate);
        }
    }

    protected void showTeamNames(String homeTeam, String awayTeam) {
        mHomeTeamName.setText(homeTeam);
        mAwayTeamName.setText(awayTeam);
    }

    public void showGroupName(String groupName) {
        if (groupName == null || groupName.isEmpty()) {
            mGroupNameLayout.setVisibility(View.GONE);
        } else {
            mGroupNameLayout.setVisibility(View.VISIBLE);
            mGroupName.setText(groupName.split(" ")[1]);
        }
    }

    public void showCurrentMinute(Integer status, Integer minute, Integer extraMinutes, String
            phaseDescription) {

        if (status != 3) {
            hideCurrentMinute();
            return;
        }

        String textToShow;
        if (minute == 0) {
            textToShow = phaseDescription;
        } else {
            textToShow = String.valueOf(minute);
            if (extraMinutes != null && extraMinutes > 0) {
                textToShow += "' " + String.valueOf(extraMinutes);
            }
        }

        mCurrentMinute.setText(textToShow);
        mCurrentMinute.setVisibility(View.VISIBLE);
    }

    private void hideCurrentMinute() {
        mCurrentMinute.setVisibility(View.GONE);
    }
}
