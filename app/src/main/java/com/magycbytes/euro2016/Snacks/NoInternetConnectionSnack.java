package com.magycbytes.euro2016.Snacks;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.magycbytes.euro2016.R;

/**
 * Created by alexandru on 6/12/16.
 */
public class NoInternetConnectionSnack {

    private final View mView;
    private final Events mEventsListener;
    private final View.OnClickListener mRetryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mEventsListener != null) {
                mEventsListener.startRefresh();
            }
        }
    };

    public NoInternetConnectionSnack(View view, Events eventsListener) {
        mView = view;
        mEventsListener = eventsListener;
    }

    public void show() {
        Snackbar snackbar = Snackbar.make(mView, R.string.no_internet_connection, Snackbar
                .LENGTH_LONG);
        snackbar.setAction(R.string.retry, mRetryClickListener);
        snackbar.show();
    }

    public interface Events {
        void startRefresh();
    }
}
