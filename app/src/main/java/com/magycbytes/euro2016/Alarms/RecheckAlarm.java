package com.magycbytes.euro2016.Alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by alexandru on 6/21/16.
 */

public class RecheckAlarm {

    public static void setAlarm(Context context, long afterTimeMilliseconds, Intent intent) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() +
                afterTimeMilliseconds, PendingIntent.getService(context, AlarmMatchesCheck
                .CHECK_TODAY_MATCHES_INTENT, intent, PendingIntent.FLAG_UPDATE_CURRENT));
    }
}
