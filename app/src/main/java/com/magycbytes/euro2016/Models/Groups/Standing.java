package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Standing {

    @SerializedName("team")
    @Expose
    private Team team;
    @SerializedName("won")
    @Expose
    private Integer won;
    @SerializedName("drawn")
    @Expose
    private Integer drawn;
    @SerializedName("lost")
    @Expose
    private Integer lost;
    @SerializedName("goalsFor")
    @Expose
    private Integer goalsFor;
    @SerializedName("goalsAgainst")
    @Expose
    private Integer goalsAgainst;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("goalDifference")
    @Expose
    private Integer goalDifference;
    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("played")
    @Expose
    private Integer played;
    @SerializedName("deductedPoints")
    @Expose
    private Integer deductedPoints;
    @SerializedName("disciplinaryTag")
    @Expose
    private String disciplinaryTag;
    @SerializedName("isLive")
    @Expose
    private Boolean isLive;

    /**
     * @return The team
     */
    public Team getTeam() {
        return team;
    }

    /**
     * @param team The team
     */
    public void setTeam(Team team) {
        this.team = team;
    }

    /**
     * @return The won
     */
    public Integer getWon() {
        return won;
    }

    /**
     * @param won The won
     */
    public void setWon(Integer won) {
        this.won = won;
    }

    /**
     * @return The drawn
     */
    public Integer getDrawn() {
        return drawn;
    }

    /**
     * @param drawn The drawn
     */
    public void setDrawn(Integer drawn) {
        this.drawn = drawn;
    }

    /**
     * @return The lost
     */
    public Integer getLost() {
        return lost;
    }

    /**
     * @param lost The lost
     */
    public void setLost(Integer lost) {
        this.lost = lost;
    }

    /**
     * @return The goalsFor
     */
    public Integer getGoalsFor() {
        return goalsFor;
    }

    /**
     * @param goalsFor The goalsFor
     */
    public void setGoalsFor(Integer goalsFor) {
        this.goalsFor = goalsFor;
    }

    /**
     * @return The goalsAgainst
     */
    public Integer getGoalsAgainst() {
        return goalsAgainst;
    }

    /**
     * @param goalsAgainst The goalsAgainst
     */
    public void setGoalsAgainst(Integer goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    /**
     * @return The points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points The points
     */
    public void setPoints(Integer points) {
        this.points = points;
    }

    /**
     * @return The goalDifference
     */
    public Integer getGoalDifference() {
        return goalDifference;
    }

    /**
     * @param goalDifference The goalDifference
     */
    public void setGoalDifference(Integer goalDifference) {
        this.goalDifference = goalDifference;
    }

    /**
     * @return The rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank The rank
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return The played
     */
    public Integer getPlayed() {
        return played;
    }

    /**
     * @param played The played
     */
    public void setPlayed(Integer played) {
        this.played = played;
    }

    /**
     * @return The deductedPoints
     */
    public Integer getDeductedPoints() {
        return deductedPoints;
    }

    /**
     * @param deductedPoints The deductedPoints
     */
    public void setDeductedPoints(Integer deductedPoints) {
        this.deductedPoints = deductedPoints;
    }

    /**
     * @return The disciplinaryTag
     */
    public String getDisciplinaryTag() {
        return disciplinaryTag;
    }

    /**
     * @param disciplinaryTag The disciplinaryTag
     */
    public void setDisciplinaryTag(String disciplinaryTag) {
        this.disciplinaryTag = disciplinaryTag;
    }

    /**
     * @return The isLive
     */
    public Boolean getIsLive() {
        return isLive;
    }

    /**
     * @param isLive The isLive
     */
    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }

}
