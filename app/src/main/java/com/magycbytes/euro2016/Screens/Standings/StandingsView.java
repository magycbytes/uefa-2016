package com.magycbytes.euro2016.Screens.Standings;

import android.view.View;

import com.magycbytes.euro2016.Screens.Helpers.ScreenView;

/**
 * Created by alexandru on 6/14/16.
 */
class StandingsView extends ScreenView {

    private final View mRootView;

    public StandingsView(View rootView) {
        super(rootView);
        mRootView = rootView;
    }

    public View getRootView() {
        return mRootView;
    }
}
