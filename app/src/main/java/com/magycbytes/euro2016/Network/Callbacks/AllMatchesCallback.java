package com.magycbytes.euro2016.Network.Callbacks;

import android.util.Log;

import com.magycbytes.euro2016.Models.AllMatches.AllMatchesResponse;
import com.magycbytes.euro2016.Screens.AllMatches.AllMatchesPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandru on 6/22/16.
 */

public class AllMatchesCallback implements Callback<AllMatchesResponse> {

    private final AllMatchesPresenter mPresenter;

    public AllMatchesCallback(AllMatchesPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResponse(Call<AllMatchesResponse> call, Response<AllMatchesResponse> response) {
        if (response.code() != 200 || response.body() == null) {
            mPresenter.showErrorMessageToUser();
            mPresenter.hideProgress();
            return;
        }

        AllMatchesResponse body = response.body();
        mPresenter.showDownloadedMatches(body.getMatchInfoItems());

        mPresenter.hideProgress();
    }

    @Override
    public void onFailure(Call<AllMatchesResponse> call, Throwable t) {
        mPresenter.hideProgress();
        Log.e(this.toString(), t.getMessage());
        mPresenter.showErrorMessageToUser();
    }
}
