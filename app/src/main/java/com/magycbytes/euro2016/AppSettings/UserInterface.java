package com.magycbytes.euro2016.AppSettings;

import android.content.Context;

/**
 * Created by alexandru on 6/16/16.
 */
public class UserInterface {

    private static final String[] mAvailableLanguages = new String[]{
            "EN", "ES", "FR", "DE", "IT", "PT", "RU", "JP", "CN"
    };

    public static String getInterfaceLanguage(Context context) {
        String displayLanguage = context.getResources().getConfiguration().locale
                .getLanguage().toUpperCase();

        for (String mAvailableLanguage : mAvailableLanguages) {
            if (mAvailableLanguage.equals(displayLanguage)) {
                return displayLanguage;
            }
        }
        return mAvailableLanguages[0];
    }
}
