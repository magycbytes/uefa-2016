package com.magycbytes.euro2016.Screens.Standings;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.euro2016.AppSettings.UserPreferences;
import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Screens.CustomViews.PhaseSelectorView;

public class StandingsFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private StandingsPresenter mStandingsPresenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_standings, container, false);

        UserPreferences.registerListener(getContext(), this);

        StandingsView view = new StandingsView(inflate);
        PhaseSelectorView standingsPhaseSelector = new PhaseSelectorView(
                inflate,
                UserPreferences.getPhaseTournamentStandings(inflate.getContext()) - 1
        );

        mStandingsPresenter = new StandingsPresenter(view, getContext(), standingsPhaseSelector);
        refresh();

        return inflate;
    }

    public void refresh() {
        mStandingsPresenter.startRefresh();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(UserPreferences.VIEW_LAYOUT)) {
            mStandingsPresenter.layoutChanged();
        }
    }
}
