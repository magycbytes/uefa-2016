package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GroupResponse {

    @SerializedName("groups")
    @Expose
    private List<Group> groups = new ArrayList<>();
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("mediaTypeId")
    @Expose
    private Object mediaTypeId;

    /**
     * @return The groups
     */
    public List<Group> getGroups() {
        return groups;
    }

    /**
     * @param groups The groups
     */
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    /**
     * @return The pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * @param pagination The pagination
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    /**
     * @return The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * @return The mediaTypeId
     */
    public Object getMediaTypeId() {
        return mediaTypeId;
    }

    /**
     * @param mediaTypeId The mediaTypeId
     */
    public void setMediaTypeId(Object mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

}
