package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("apiVersion")
    @Expose
    private String apiVersion;
    @SerializedName("generatedAt")
    @Expose
    private String generatedAt;
    @SerializedName("requestedBy")
    @Expose
    private String requestedBy;
    @SerializedName("deprecationWarning")
    @Expose
    private DeprecationWarning deprecationWarning;

    /**
     * @return The apiVersion
     */
    public String getApiVersion() {
        return apiVersion;
    }

    /**
     * @param apiVersion The apiVersion
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    /**
     * @return The generatedAt
     */
    public String getGeneratedAt() {
        return generatedAt;
    }

    /**
     * @param generatedAt The generatedAt
     */
    public void setGeneratedAt(String generatedAt) {
        this.generatedAt = generatedAt;
    }

    /**
     * @return The requestedBy
     */
    public String getRequestedBy() {
        return requestedBy;
    }

    /**
     * @param requestedBy The requestedBy
     */
    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    /**
     * @return The deprecationWarning
     */
    public DeprecationWarning getDeprecationWarning() {
        return deprecationWarning;
    }

    /**
     * @param deprecationWarning The deprecationWarning
     */
    public void setDeprecationWarning(DeprecationWarning deprecationWarning) {
        this.deprecationWarning = deprecationWarning;
    }

}
