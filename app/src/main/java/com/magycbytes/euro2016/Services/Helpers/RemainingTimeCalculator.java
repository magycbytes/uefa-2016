package com.magycbytes.euro2016.Services.Helpers;

import android.util.Log;

import com.magycbytes.euro2016.Screens.TodayMatches.Helpers.MatchHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexandru on 6/21/16.
 */

public class RemainingTimeCalculator {

    private static final int ONE_HOUR_WITHOUT_FIVE_MINUTES = 55;
    private long mOneHourDifference;
    private long mBeforeStartWithFiveMinutes;

    public boolean calculateTime(String dateAndTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MatchHolder
                .DATE_TIME_FORMAT, Locale.getDefault());

        Date date;
        try {
            date = simpleDateFormat.parse(dateAndTime);
        } catch (ParseException e) {
            Log.e(this.toString(), e.getMessage());
            return false;
        }
        Date oneHourBeforeStart = new Date(date.getTime() - TimeUnit.HOURS.toMillis(1));
        Date currentDate = Calendar.getInstance().getTime();
        mOneHourDifference = oneHourBeforeStart.getTime() - currentDate.getTime();
        mBeforeStartWithFiveMinutes = mOneHourDifference + TimeUnit.MINUTES.toMillis(ONE_HOUR_WITHOUT_FIVE_MINUTES);

        return true;
    }

    public boolean isMoreThanOneHourToWait() {
        return mOneHourDifference > TimeUnit.HOURS.toMillis(1);
    }

    public boolean isLessThanOneHourToWait() {
        return mOneHourDifference > 0;
    }

    public boolean willBeFiveMinutesBefore() {
        return mBeforeStartWithFiveMinutes > 0;
    }

    public long getOneHourDifference() {
        return mOneHourDifference;
    }

    public long getBeforeStartWithFiveMinutes() {
        return mBeforeStartWithFiveMinutes;
    }
}
