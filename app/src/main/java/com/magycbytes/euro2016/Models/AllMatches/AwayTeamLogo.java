
package com.magycbytes.euro2016.Models.AllMatches;

import java.util.HashMap;
import java.util.Map;


public class AwayTeamLogo {

    private String logo70;
    private String logo240;
    private String logo700;
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The logo70
     */
    public String getLogo70() {
        return logo70;
    }

    /**
     * 
     * @param logo70
     *     The logo70
     */
    public void setLogo70(String logo70) {
        this.logo70 = logo70;
    }

    /**
     * 
     * @return
     *     The logo240
     */
    public String getLogo240() {
        return logo240;
    }

    /**
     * 
     * @param logo240
     *     The logo240
     */
    public void setLogo240(String logo240) {
        this.logo240 = logo240;
    }

    /**
     * 
     * @return
     *     The logo700
     */
    public String getLogo700() {
        return logo700;
    }

    /**
     * 
     * @param logo700
     *     The logo700
     */
    public void setLogo700(String logo700) {
        this.logo700 = logo700;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
