package com.magycbytes.euro2016.Alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by alexandru on 6/21/16.
 */

public class AlarmMatchesCheck {

    public static final int CHECK_TODAY_MATCHES_INTENT = 1;
    private static final String TAG = "AlarmMatchesCheck";

    public static void setAlarm(Context context, Intent intent) {

        if (isAlarmAlreadySet(context, intent)) {
            Log.i(TAG, "Alarm already exists"); //NON-NLS
            return;
        }
        Log.i(TAG, "Alarm do not exist. Setting new one"); //NON-NLS

        setOneDayRepeat(context, intent);

//        context.startService(intent);
    }

    private static void setOneDayRepeat(Context context, Intent intent) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 7);

        PendingIntent pendingIntent = PendingIntent.getService(context, CHECK_TODAY_MATCHES_INTENT,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
                pendingIntent);
    }

    private static boolean isAlarmAlreadySet(Context context, Intent intent) {

        PendingIntent pendingIntent = PendingIntent.getService(context, CHECK_TODAY_MATCHES_INTENT, intent, PendingIntent
                .FLAG_NO_CREATE);
        return pendingIntent != null;
    }
}
