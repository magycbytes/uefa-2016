package com.magycbytes.euro2016.Screens.PlayersRank;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.magycbytes.euro2016.Screens.Helpers.ScreenView;
import com.magycbytes.euro2016.Screens.PlayersRank.helpers.ColumnsCalculator;

/**
 * Created by alexandru on 6/15/16.
 */
class PlayersRankView extends ScreenView {

    PlayersRankView(View rootView) {
        super(rootView);

        Context context = rootView.getContext();
        setLayoutManager(new GridLayoutManager(context, ColumnsCalculator.getColumnsNumber(context)));
    }
}
