package com.magycbytes.euro2016.Services;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import com.magycbytes.euro2016.Alarms.NotificationAlarm;
import com.magycbytes.euro2016.Alarms.RecheckAlarm;
import com.magycbytes.euro2016.AppSettings.InternetChecker;
import com.magycbytes.euro2016.AppSettings.UserInterface;
import com.magycbytes.euro2016.Models.TodayMatches.MatchInfoItem;
import com.magycbytes.euro2016.Models.TodayMatches.MatchesResponse;
import com.magycbytes.euro2016.Network.ManagerApi;
import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Receivers.ConnectionReceiver;
import com.magycbytes.euro2016.Services.Helpers.MessageCreator;
import com.magycbytes.euro2016.Services.Helpers.RemainingTimeCalculator;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

public class TodayMatchesNotificationService extends IntentService {

    private static final String TITLE_NOTIFICATION_FORMAT = "%s vs %s";
    private static final String MATCH_FORMAT = "%s %s";
    private List<MatchInfoItem> mMatchInfoItems;

    public TodayMatchesNotificationService() {
        super("TodayMatchesNotificationService");
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, TodayMatchesNotificationService.class);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Context context = getApplicationContext();

        if (InternetChecker.isNotConnectedToInternet(getApplicationContext())) {
            startListenerForConnection();
            return;
        }
        if (!extractMatches()) {
            RecheckAlarm.setAlarm(context, TimeUnit.MINUTES.toMillis(15), getIntent(context));
            return;
        }

        for (int i = 0; i < mMatchInfoItems.size(); ++i) {
            String title = String.format(
                    TITLE_NOTIFICATION_FORMAT,
                    mMatchInfoItems.get(i).getHomeTeamName(),
                    mMatchInfoItems.get(i).getAwayTeamName()
            );

            RemainingTimeCalculator calculator = new RemainingTimeCalculator();
            if (!calculator.calculateTime(mMatchInfoItems.get(i).getDateTime())) {
                continue;
            }

            if (calculator.isMoreThanOneHourToWait()) {
                String message = String.format(
                        Locale.getDefault(),
                        MATCH_FORMAT,
                        getString(R.string.will_start),
                        getString(R.string.in_one_hour));

                NotificationAlarm.setAlarm(context, calculator.getOneHourDifference(), message, title);
            } else if (calculator.isLessThanOneHourToWait()) {
                String message = getString(R.string.will_start) +
                        MessageCreator.getMessage(context, calculator.getOneHourDifference());
                NotificationAlarm.setAlarm(context, calculator.getOneHourDifference(), message, title);
            }

            if (calculator.willBeFiveMinutesBefore()) {
                String messageMinutes = String.format(
                        MATCH_FORMAT,
                        getString(R.string.will_start),
                        getString(R.string.in_5_minutes));
                NotificationAlarm.setAlarm(context, calculator.getBeforeStartWithFiveMinutes(),
                        messageMinutes, title);
            }
        }
    }

    private boolean extractMatches() {
        String interfaceLanguage = UserInterface.getInterfaceLanguage(getApplicationContext());
        Call<MatchesResponse> todayMatches = ManagerApi.getApi(getApplicationContext())
                .getTodayMatches(interfaceLanguage);
        try {
            Response<MatchesResponse> responseResponse = todayMatches.execute();
            mMatchInfoItems = responseResponse.body().getMatchInfoItems();
        } catch (IOException e) {
            Log.e(this.toString(), e.getMessage());
            return false;
        }
        return true;
    }

    private void startListenerForConnection() {
        ComponentName componentName = new ComponentName(getApplicationContext(),
                ConnectionReceiver.class);
        getApplicationContext().getPackageManager().setComponentEnabledSetting(
                componentName,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP
        );
    }
}
