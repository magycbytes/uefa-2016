package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerRankResponse {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private List<PlayersRanking> playersRanking = new ArrayList<>();
    private RowtArticle rowtArticle;
    private List<RankingsEvolution> rankingsEvolution = new ArrayList<>();
    private Pagination pagination;
    private Meta meta;
    private Integer mediaTypeId;

    /**
     * @return The playersRanking
     */
    public List<PlayersRanking> getPlayersRanking() {
        return playersRanking;
    }

    /**
     * @param playersRanking The playersRanking
     */
    public void setPlayersRanking(List<PlayersRanking> playersRanking) {
        this.playersRanking = playersRanking;
    }

    /**
     * @return The rowtArticle
     */
    public RowtArticle getRowtArticle() {
        return rowtArticle;
    }

    /**
     * @param rowtArticle The rowtArticle
     */
    public void setRowtArticle(RowtArticle rowtArticle) {
        this.rowtArticle = rowtArticle;
    }

    /**
     * @return The rankingsEvolution
     */
    public List<RankingsEvolution> getRankingsEvolution() {
        return rankingsEvolution;
    }

    /**
     * @param rankingsEvolution The rankingsEvolution
     */
    public void setRankingsEvolution(List<RankingsEvolution> rankingsEvolution) {
        this.rankingsEvolution = rankingsEvolution;
    }

    /**
     * @return The pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * @param pagination The pagination
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    /**
     * @return The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * @return The mediaTypeId
     */
    public Integer getMediaTypeId() {
        return mediaTypeId;
    }

    /**
     * @param mediaTypeId The mediaTypeId
     */
    public void setMediaTypeId(Integer mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
