
package com.magycbytes.euro2016.Models.AllMatches;

import java.util.HashMap;
import java.util.Map;


public class Results {

    private Integer idWinnerTeam;
    private Integer homeGoals;
    private Integer awayGoals;
    private Integer homePenGoals;
    private Integer awayPenGoals;
    private Integer homeAggGoals;
    private Integer awayAggGoals;
    private Integer homeSuspScore;
    private Integer awaySuspScore;
    private Integer homeSuspPenScore;
    private Integer awaySuspPenScore;
    private Integer homeSuspAggScore;
    private Integer awaySuspAggScore;
    private String reasonWinTag;
    private Integer reasonWin;
    private Boolean hasAggregate;
    private Integer status;
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The idWinnerTeam
     */
    public Integer getIdWinnerTeam() {
        return idWinnerTeam;
    }

    /**
     * 
     * @param idWinnerTeam
     *     The idWinnerTeam
     */
    public void setIdWinnerTeam(Integer idWinnerTeam) {
        this.idWinnerTeam = idWinnerTeam;
    }

    /**
     * 
     * @return
     *     The homeGoals
     */
    public Integer getHomeGoals() {
        return homeGoals;
    }

    /**
     * 
     * @param homeGoals
     *     The homeGoals
     */
    public void setHomeGoals(Integer homeGoals) {
        this.homeGoals = homeGoals;
    }

    /**
     * 
     * @return
     *     The awayGoals
     */
    public Integer getAwayGoals() {
        return awayGoals;
    }

    /**
     * 
     * @param awayGoals
     *     The awayGoals
     */
    public void setAwayGoals(Integer awayGoals) {
        this.awayGoals = awayGoals;
    }

    /**
     * 
     * @return
     *     The homePenGoals
     */
    public Integer getHomePenGoals() {
        return homePenGoals;
    }

    /**
     * 
     * @param homePenGoals
     *     The homePenGoals
     */
    public void setHomePenGoals(Integer homePenGoals) {
        this.homePenGoals = homePenGoals;
    }

    /**
     * 
     * @return
     *     The awayPenGoals
     */
    public Integer getAwayPenGoals() {
        return awayPenGoals;
    }

    /**
     * 
     * @param awayPenGoals
     *     The awayPenGoals
     */
    public void setAwayPenGoals(Integer awayPenGoals) {
        this.awayPenGoals = awayPenGoals;
    }

    /**
     * 
     * @return
     *     The homeAggGoals
     */
    public Integer getHomeAggGoals() {
        return homeAggGoals;
    }

    /**
     * 
     * @param homeAggGoals
     *     The homeAggGoals
     */
    public void setHomeAggGoals(Integer homeAggGoals) {
        this.homeAggGoals = homeAggGoals;
    }

    /**
     * 
     * @return
     *     The awayAggGoals
     */
    public Integer getAwayAggGoals() {
        return awayAggGoals;
    }

    /**
     * 
     * @param awayAggGoals
     *     The awayAggGoals
     */
    public void setAwayAggGoals(Integer awayAggGoals) {
        this.awayAggGoals = awayAggGoals;
    }

    /**
     * 
     * @return
     *     The homeSuspScore
     */
    public Integer getHomeSuspScore() {
        return homeSuspScore;
    }

    /**
     * 
     * @param homeSuspScore
     *     The homeSuspScore
     */
    public void setHomeSuspScore(Integer homeSuspScore) {
        this.homeSuspScore = homeSuspScore;
    }

    /**
     * 
     * @return
     *     The awaySuspScore
     */
    public Integer getAwaySuspScore() {
        return awaySuspScore;
    }

    /**
     * 
     * @param awaySuspScore
     *     The awaySuspScore
     */
    public void setAwaySuspScore(Integer awaySuspScore) {
        this.awaySuspScore = awaySuspScore;
    }

    /**
     * 
     * @return
     *     The homeSuspPenScore
     */
    public Integer getHomeSuspPenScore() {
        return homeSuspPenScore;
    }

    /**
     * 
     * @param homeSuspPenScore
     *     The homeSuspPenScore
     */
    public void setHomeSuspPenScore(Integer homeSuspPenScore) {
        this.homeSuspPenScore = homeSuspPenScore;
    }

    /**
     * 
     * @return
     *     The awaySuspPenScore
     */
    public Integer getAwaySuspPenScore() {
        return awaySuspPenScore;
    }

    /**
     * 
     * @param awaySuspPenScore
     *     The awaySuspPenScore
     */
    public void setAwaySuspPenScore(Integer awaySuspPenScore) {
        this.awaySuspPenScore = awaySuspPenScore;
    }

    /**
     * 
     * @return
     *     The homeSuspAggScore
     */
    public Integer getHomeSuspAggScore() {
        return homeSuspAggScore;
    }

    /**
     * 
     * @param homeSuspAggScore
     *     The homeSuspAggScore
     */
    public void setHomeSuspAggScore(Integer homeSuspAggScore) {
        this.homeSuspAggScore = homeSuspAggScore;
    }

    /**
     * 
     * @return
     *     The awaySuspAggScore
     */
    public Integer getAwaySuspAggScore() {
        return awaySuspAggScore;
    }

    /**
     * 
     * @param awaySuspAggScore
     *     The awaySuspAggScore
     */
    public void setAwaySuspAggScore(Integer awaySuspAggScore) {
        this.awaySuspAggScore = awaySuspAggScore;
    }

    /**
     * 
     * @return
     *     The reasonWinTag
     */
    public String getReasonWinTag() {
        return reasonWinTag;
    }

    /**
     * 
     * @param reasonWinTag
     *     The reasonWinTag
     */
    public void setReasonWinTag(String reasonWinTag) {
        this.reasonWinTag = reasonWinTag;
    }

    /**
     * 
     * @return
     *     The reasonWin
     */
    public Integer getReasonWin() {
        return reasonWin;
    }

    /**
     * 
     * @param reasonWin
     *     The reasonWin
     */
    public void setReasonWin(Integer reasonWin) {
        this.reasonWin = reasonWin;
    }

    /**
     * 
     * @return
     *     The hasAggregate
     */
    public Boolean getHasAggregate() {
        return hasAggregate;
    }

    /**
     * 
     * @param hasAggregate
     *     The hasAggregate
     */
    public void setHasAggregate(Boolean hasAggregate) {
        this.hasAggregate = hasAggregate;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
