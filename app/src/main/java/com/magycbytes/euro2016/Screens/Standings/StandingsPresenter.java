package com.magycbytes.euro2016.Screens.Standings;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.magycbytes.euro2016.AppSettings.InternetChecker;
import com.magycbytes.euro2016.AppSettings.UserInterface;
import com.magycbytes.euro2016.AppSettings.UserPreferences;
import com.magycbytes.euro2016.Models.Groups.Group;
import com.magycbytes.euro2016.Models.Groups.GroupResponse;
import com.magycbytes.euro2016.Models.Groups.Standing;
import com.magycbytes.euro2016.Network.Callbacks.StandingsCallback;
import com.magycbytes.euro2016.Network.ManagerApi;
import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Screens.CustomViews.PhaseSelectorView;
import com.magycbytes.euro2016.Screens.Standings.Adapters.GroupListCardAdapter;
import com.magycbytes.euro2016.Screens.Standings.Adapters.GroupsListAdapter;
import com.magycbytes.euro2016.Snacks.NoInternetConnectionSnack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

/**
 * Created by alexandru on 6/14/16.
 */
public class StandingsPresenter implements NoInternetConnectionSnack.Events, AdapterView.OnItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private final StandingsView mView;
    private final Context mContext;
    private final NoInternetConnectionSnack mConnectionSnack;
    private Map<Integer, Object> mAdapterData;

    public StandingsPresenter(StandingsView view, Context context, PhaseSelectorView standingsPhaseSelector) {
        mView = view;
        mContext = context;
        mConnectionSnack = new NoInternetConnectionSnack(view.getRootView(), this);

        standingsPhaseSelector.setOnItemSelected(this);
        mView.setRefreshListener(this);
    }

    public void showDownloadedGroups(List<Group> groups) {
        mAdapterData = new HashMap<>(groups.size() * 4);
        int index = 0;

        for (int i = 0; i < groups.size(); ++i) {
            mAdapterData.put(index++, groups.get(i).getGroupName());
            List<Standing> teams = groups.get(i).getStandings();
            for (int j = 0; j < teams.size(); ++j) {
                mAdapterData.put(index++, teams.get(j));
            }
        }

        showData();
    }

    private void showProgress() {
        mView.showProgress();
    }

    public void hideProgress() {
        mView.hideProgress();
    }

    public void layoutChanged() {
        if (mAdapterData == null) {
            return;
        }
        showData();
    }

    @Override
    public void startRefresh() {
        onRefresh();
    }

    public void showErrorMessageToUser() {
        Toast.makeText(mContext, R.string.error_communicating_with_server, Toast.LENGTH_SHORT)
                .show();
    }

    private void showData() {
        RecyclerView.Adapter adapter;

        switch (UserPreferences.getViewPreference(mContext)) {
            case UserPreferences.TABLE_VIEW:
                adapter = new GroupsListAdapter(mAdapterData);
                break;
            case UserPreferences.CARD_VIEW:
                adapter = new GroupListCardAdapter(mAdapterData, mContext);
                break;
            default:
                adapter = new GroupsListAdapter(mAdapterData);
        }

        mView.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (view == null) {
            return;
        }
        UserPreferences.savePhaseTournamentStandings(view.getContext(), i + 1);
        startRefresh();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onRefresh() {
        // TODO check if not already running
        if (InternetChecker.isNotConnectedToInternet(mContext)) {
            mConnectionSnack.show();
            return;
        }

        showProgress();

        Call<GroupResponse> call = ManagerApi.getApi(mContext).getGroups(UserInterface
                .getInterfaceLanguage(mContext), UserPreferences.getPhaseTournamentStandings(mContext));
        call.enqueue(new StandingsCallback(this));
    }
}
