package com.magycbytes.euro2016.Screens.AllMatches.Helpers;

import com.magycbytes.euro2016.Models.AllMatches.MatchInfoItem;
import com.magycbytes.euro2016.Models.AllMatches.MatchList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alexandru on 6/23/16.
 */

public class AllMatchesResponseFilter {

    private final HashMap<String, List<MatchList>> mappedElements = new HashMap<>();
    private final List<String> mKeys = new ArrayList<>();

    public void filter(List<MatchInfoItem> matchInfoItems) {
        for (int i = 0; i < matchInfoItems.size(); ++i) {
            List<MatchList> matchList = matchInfoItems.get(i).getMatchList();
            filterOneListOfItems(matchList);
        }
    }

    public List<Object> getAdapterData(String lastDate) {
        AllMatchesToAdapterConverter adapterConverter = new AllMatchesToAdapterConverter();
        return adapterConverter.convert(
                mappedElements,
                mKeys,
                lastDate
        );
    }

    public String getLastDate() {
        if (mKeys.isEmpty()) {
            return null;
        }
        return mKeys.get(mKeys.size() - 1);
    }

    public HashMap<String, List<MatchList>> getMappedElements() {
        return mappedElements;
    }

    public List<String> getKeys() {
        return mKeys;
    }

    private void filterOneListOfItems(List<MatchList> matchList) {
        for (int j = 0; j < matchList.size(); ++j) {
            String date = matchList.get(j).getDate();
            if (mappedElements.get(date) == null) {
                addNewMatch(date, matchList.get(j));
            } else {
                attachMatchToExistingDate(date, matchList.get(j));
            }
        }
    }

    private void addNewMatch(String date, MatchList matchList) {
        ArrayList<MatchList> newMatchList = new ArrayList<>();
        newMatchList.add(matchList);
        mappedElements.put(date, newMatchList);
        mKeys.add(date);
    }

    private void attachMatchToExistingDate(String date, MatchList matchList) {
        List<MatchList> existingMatches = mappedElements.get(date);
        existingMatches.add(matchList);
    }
}
