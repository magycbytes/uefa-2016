package com.magycbytes.euro2016.Receivers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.util.Log;

import com.magycbytes.euro2016.AppSettings.InternetChecker;
import com.magycbytes.euro2016.Services.TodayMatchesNotificationService;

public class ConnectionReceiver extends BroadcastReceiver {
    public ConnectionReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            return;
        }

        Log.i(this.toString(), "On connection receiver"); //NON-NLS
        if (InternetChecker.isNotConnectedToInternet(context)) {
            return;
        }

        Intent service = TodayMatchesNotificationService.getIntent(context);
        context.startService(service);

        disableSelf(context);
    }

    private void disableSelf(Context context) {
        ComponentName componentName = new ComponentName(context, ConnectionReceiver.class);

        context.getPackageManager().setComponentEnabledSetting(componentName,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
