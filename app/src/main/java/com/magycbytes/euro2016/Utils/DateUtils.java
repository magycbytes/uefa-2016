package com.magycbytes.euro2016.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by alexandru on 6/23/16.
 */

public class DateUtils {

    public static final String SIMPLE_DATE_FORMAT = "dd/MM/yyyy";
    private static final String DAY_DATE_MONTH_FORMAT = "EEEE dd LLLL yyyy";

    public static String getFormattedDayFromString(String oldDate, String oldFormat) throws ParseException {
        return getFormattedString(getDate(oldDate, oldFormat));
    }

    private static Date getDate(String dateString, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return simpleDateFormat.parse(dateString);
    }

    private static String getFormattedString(Date date) {
        return new SimpleDateFormat(DateUtils.DAY_DATE_MONTH_FORMAT, Locale.getDefault()).format(date);
    }
}
