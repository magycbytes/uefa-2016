package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pagination {

    @SerializedName("nextUrl")
    @Expose
    private Object nextUrl;
    @SerializedName("previousUrl")
    @Expose
    private Object previousUrl;
    @SerializedName("maxItems")
    @Expose
    private Integer maxItems;

    /**
     * @return The nextUrl
     */
    public Object getNextUrl() {
        return nextUrl;
    }

    /**
     * @param nextUrl The nextUrl
     */
    public void setNextUrl(Object nextUrl) {
        this.nextUrl = nextUrl;
    }

    /**
     * @return The previousUrl
     */
    public Object getPreviousUrl() {
        return previousUrl;
    }

    /**
     * @param previousUrl The previousUrl
     */
    public void setPreviousUrl(Object previousUrl) {
        this.previousUrl = previousUrl;
    }

    /**
     * @return The maxItems
     */
    public Integer getMaxItems() {
        return maxItems;
    }

    /**
     * @param maxItems The maxItems
     */
    public void setMaxItems(Integer maxItems) {
        this.maxItems = maxItems;
    }

}
