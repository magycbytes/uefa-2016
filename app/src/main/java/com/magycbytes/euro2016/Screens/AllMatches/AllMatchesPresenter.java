package com.magycbytes.euro2016.Screens.AllMatches;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.magycbytes.euro2016.AppSettings.UserInterface;
import com.magycbytes.euro2016.AppSettings.UserPreferences;
import com.magycbytes.euro2016.Models.AllMatches.AllMatchesResponse;
import com.magycbytes.euro2016.Models.AllMatches.MatchInfoItem;
import com.magycbytes.euro2016.Network.Callbacks.AllMatchesCallback;
import com.magycbytes.euro2016.Network.ManagerApi;
import com.magycbytes.euro2016.Screens.AllMatches.Helpers.AllMatchesResponseFilter;
import com.magycbytes.euro2016.Screens.AllMatches.Helpers.RecyclerScrollListener;
import com.magycbytes.euro2016.Screens.CustomViews.PhaseSelectorView;
import com.magycbytes.euro2016.Screens.Helpers.ScreenPresenter;

import java.util.List;

import retrofit2.Call;

/**
 * Created by alexandru on 6/22/16.
 */

public class AllMatchesPresenter extends ScreenPresenter implements RecyclerScrollListener.Events, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener {

    private String lastDate = null;
    private final AllMatchesView mMatchesView;

    AllMatchesPresenter(Context context, AllMatchesView matchesView, PhaseSelectorView phaseSelectorView) {
        super(context, matchesView);
        mMatchesView = matchesView;

        matchesView.setRefreshListener(this);
        phaseSelectorView.setOnItemSelected(this);
    }

    public void showDownloadedMatches(List<MatchInfoItem> matchInfoItems) {
        if (matchInfoItems.isEmpty()) {
            Log.i(this.toString(), "Is empty");
            mMatchesView.clearScrollListener();
            return;
        }
        Log.i(this.toString(), "Showing downloaded data");

        AllMatchesResponseFilter matchesFilter = new AllMatchesResponseFilter();
        matchesFilter.filter(matchInfoItems);

        mMatchesView.addItems(matchesFilter.getAdapterData(lastDate));
        lastDate = matchesFilter.getLastDate();
    }

    @Override
    public void loadMoreItems() {
        super.refresh();

        int startDownloadFrom = mScreenView.getItemsCountInList();
        String language = UserInterface.getInterfaceLanguage(mContext);
        int phaseStanding = UserPreferences.getPhaseTournamentAllMatches(mContext);

        Log.i(this.toString(), "Loading phase: " + phaseStanding);

        Call<AllMatchesResponse> allMatches = ManagerApi.getApi(mContext).getAllMatches(
                language, phaseStanding, startDownloadFrom);
        allMatches.enqueue(new AllMatchesCallback(this));
    }

    @Override
    public void onRefresh() {
        super.refresh();

        lastDate = null;
        mMatchesView.clearItems();
        mMatchesView.setEventsListener(this);
        mMatchesView.addScrollListener();

        loadMoreItems();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.i(this.toString(), "On item selected");
        UserPreferences.savePhaseTournamentAllMatches(mContext, i + 1);
        mMatchesView.addScrollListener();
        onRefresh();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
