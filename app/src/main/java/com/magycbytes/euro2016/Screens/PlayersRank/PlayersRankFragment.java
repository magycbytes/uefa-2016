package com.magycbytes.euro2016.Screens.PlayersRank;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.euro2016.R;

public class PlayersRankFragment extends Fragment {

    private PlayersRankPresenter mRankPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players_rank, container, false);

        PlayersRankView rankView = new PlayersRankView(view);
        mRankPresenter = new PlayersRankPresenter(rankView, getContext());
        mRankPresenter.onRefresh();

        return view;
    }

    public void refresh() {
        mRankPresenter.onRefresh();
    }
}
