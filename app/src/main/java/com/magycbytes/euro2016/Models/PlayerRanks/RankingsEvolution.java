package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RankingsEvolution {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String date;
    private Long lastModified;
    private String position;
    private List<Player> players = new ArrayList<>();

    /**
     * @return The date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The lastModified
     */
    public Long getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified The lastModified
     */
    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @return The position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position The position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return The players
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     * @param players The players
     */
    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
