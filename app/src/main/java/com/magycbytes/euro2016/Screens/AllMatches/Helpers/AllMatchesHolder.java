package com.magycbytes.euro2016.Screens.AllMatches.Helpers;

import android.view.View;

import com.magycbytes.euro2016.Models.AllMatches.MatchList;
import com.magycbytes.euro2016.Screens.TodayMatches.Helpers.MatchHolder;

/**
 * Created by alexandru on 6/24/16.
 */

class AllMatchesHolder extends MatchHolder {

    public AllMatchesHolder(View itemView) {
        super(itemView);
    }

    public void showMatch(MatchList matchList) {
        loadFlags(mContext,
                matchList.getHomeTeamLogo().getLogo70(),
                matchList.getAwayTeamLogo().getLogo70());

        showTeamNames(matchList.getHomeTeamName(), matchList.getAwayTeamName());
        showGroupName(matchList.getGroupName());

        showGoals(
                matchList.getResults().getHomeGoals(),
                matchList.getResults().getAwayGoals(),
                matchList.getDateTime()
        );

        showCurrentMinute(
                matchList.getStatus(),
                matchList.getMinute(),
                matchList.getMinuteextra(),
                matchList.getMatchPhaseDetailed()
        );
    }
}
