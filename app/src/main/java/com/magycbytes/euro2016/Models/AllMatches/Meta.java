
package com.magycbytes.euro2016.Models.AllMatches;

import java.util.HashMap;
import java.util.Map;


public class Meta {

    private String apiVersion;
    private String generatedAt;
    private String requestedBy;
    private DeprecationWarning deprecationWarning;
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The apiVersion
     */
    public String getApiVersion() {
        return apiVersion;
    }

    /**
     * 
     * @param apiVersion
     *     The apiVersion
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    /**
     * 
     * @return
     *     The generatedAt
     */
    public String getGeneratedAt() {
        return generatedAt;
    }

    /**
     * 
     * @param generatedAt
     *     The generatedAt
     */
    public void setGeneratedAt(String generatedAt) {
        this.generatedAt = generatedAt;
    }

    /**
     * 
     * @return
     *     The requestedBy
     */
    public String getRequestedBy() {
        return requestedBy;
    }

    /**
     * 
     * @param requestedBy
     *     The requestedBy
     */
    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    /**
     * 
     * @return
     *     The deprecationWarning
     */
    public DeprecationWarning getDeprecationWarning() {
        return deprecationWarning;
    }

    /**
     * 
     * @param deprecationWarning
     *     The deprecationWarning
     */
    public void setDeprecationWarning(DeprecationWarning deprecationWarning) {
        this.deprecationWarning = deprecationWarning;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
