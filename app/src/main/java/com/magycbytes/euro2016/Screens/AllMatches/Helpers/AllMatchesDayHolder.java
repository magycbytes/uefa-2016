package com.magycbytes.euro2016.Screens.AllMatches.Helpers;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Utils.DateUtils;

import java.text.ParseException;

/**
 * Created by alexandru on 6/23/16.
 */

class AllMatchesDayHolder extends RecyclerView.ViewHolder {

    private final TextView mMatchDay;

    AllMatchesDayHolder(View itemView) {
        super(itemView);

        mMatchDay = (TextView) itemView.findViewById(R.id.matchDay);
    }

    void setMatchDay(String date) {

        String dateToShow = date;
        try {
            dateToShow = DateUtils.getFormattedDayFromString(
                    dateToShow,
                    DateUtils.SIMPLE_DATE_FORMAT
            );
        } catch (ParseException e) {
            Log.e(this.toString(), e.getMessage());
        }

        mMatchDay.setText(dateToShow);
    }
}
