package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PenaltyShootout {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private List<PenaltyShootoutHome> penaltyShootoutHome = new ArrayList<>();
    private List<PenaltyShootoutAway> penaltyShootoutAway = new ArrayList<>();

    /**
     * @return The penaltyShootoutHome
     */
    public List<PenaltyShootoutHome> getPenaltyShootoutHome() {
        return penaltyShootoutHome;
    }

    /**
     * @param penaltyShootoutHome The penaltyShootoutHome
     */
    public void setPenaltyShootoutHome(List<PenaltyShootoutHome> penaltyShootoutHome) {
        this.penaltyShootoutHome = penaltyShootoutHome;
    }

    /**
     * @return The penaltyShootoutAway
     */
    public List<PenaltyShootoutAway> getPenaltyShootoutAway() {
        return penaltyShootoutAway;
    }

    /**
     * @param penaltyShootoutAway The penaltyShootoutAway
     */
    public void setPenaltyShootoutAway(List<PenaltyShootoutAway> penaltyShootoutAway) {
        this.penaltyShootoutAway = penaltyShootoutAway;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
