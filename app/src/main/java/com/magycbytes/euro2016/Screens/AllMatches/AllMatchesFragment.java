package com.magycbytes.euro2016.Screens.AllMatches;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.euro2016.AppSettings.UserPreferences;
import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Screens.CustomViews.PhaseSelectorView;

public class AllMatchesFragment extends Fragment {

    private AllMatchesPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_matches, container, false);

        AllMatchesView allMatchesView = new AllMatchesView(view);
        PhaseSelectorView phaseSelectorView = new PhaseSelectorView(
                view,
                UserPreferences.getPhaseTournamentAllMatches(view.getContext()) - 1
        );

        mPresenter = new AllMatchesPresenter(getContext(), allMatchesView, phaseSelectorView);
//        refresh();

        return view;
    }

    public void refresh() {
        mPresenter.onRefresh();
    }
}
