package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.HashMap;
import java.util.Map;

class ParsedBody {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String type;

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
