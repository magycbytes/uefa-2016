package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RowtArticle {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String body;
    private List<ParsedBody> parsedBody = new ArrayList<>();
    private List<Medium> media = new ArrayList<>();
    private String sharingUrl;
    private Integer mediaCount;
    private List<RelatedTag> relatedTags = new ArrayList<>();
    private String href;
    private Integer mediaTypeId;
    private String headline;
    private String comment;
    private String publicationDate;
    private Image_ image;
    private Integer id;
    private Integer idCup;
    private Integer idCupSeason;
    private String copyright;
    private String hdsUrl;
    private String hlsUrl;
    private Integer durationVideo;

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The parsedBody
     */
    public List<ParsedBody> getParsedBody() {
        return parsedBody;
    }

    /**
     * @param parsedBody The parsedBody
     */
    public void setParsedBody(List<ParsedBody> parsedBody) {
        this.parsedBody = parsedBody;
    }

    /**
     * @return The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * @param media The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    /**
     * @return The sharingUrl
     */
    public String getSharingUrl() {
        return sharingUrl;
    }

    /**
     * @param sharingUrl The sharingUrl
     */
    public void setSharingUrl(String sharingUrl) {
        this.sharingUrl = sharingUrl;
    }

    /**
     * @return The mediaCount
     */
    public Integer getMediaCount() {
        return mediaCount;
    }

    /**
     * @param mediaCount The mediaCount
     */
    public void setMediaCount(Integer mediaCount) {
        this.mediaCount = mediaCount;
    }

    /**
     * @return The relatedTags
     */
    public List<RelatedTag> getRelatedTags() {
        return relatedTags;
    }

    /**
     * @param relatedTags The relatedTags
     */
    public void setRelatedTags(List<RelatedTag> relatedTags) {
        this.relatedTags = relatedTags;
    }

    /**
     * @return The href
     */
    public String getHref() {
        return href;
    }

    /**
     * @param href The href
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * @return The mediaTypeId
     */
    public Integer getMediaTypeId() {
        return mediaTypeId;
    }

    /**
     * @param mediaTypeId The mediaTypeId
     */
    public void setMediaTypeId(Integer mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    /**
     * @return The headline
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * @param headline The headline
     */
    public void setHeadline(String headline) {
        this.headline = headline;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The publicationDate
     */
    public String getPublicationDate() {
        return publicationDate;
    }

    /**
     * @param publicationDate The publicationDate
     */
    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    /**
     * @return The image
     */
    public Image_ getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(Image_ image) {
        this.image = image;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The idCup
     */
    public Integer getIdCup() {
        return idCup;
    }

    /**
     * @param idCup The idCup
     */
    public void setIdCup(Integer idCup) {
        this.idCup = idCup;
    }

    /**
     * @return The idCupSeason
     */
    public Integer getIdCupSeason() {
        return idCupSeason;
    }

    /**
     * @param idCupSeason The idCupSeason
     */
    public void setIdCupSeason(Integer idCupSeason) {
        this.idCupSeason = idCupSeason;
    }

    /**
     * @return The copyright
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * @param copyright The copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * @return The hdsUrl
     */
    public String getHdsUrl() {
        return hdsUrl;
    }

    /**
     * @param hdsUrl The hdsUrl
     */
    public void setHdsUrl(String hdsUrl) {
        this.hdsUrl = hdsUrl;
    }

    /**
     * @return The hlsUrl
     */
    public String getHlsUrl() {
        return hlsUrl;
    }

    /**
     * @param hlsUrl The hlsUrl
     */
    public void setHlsUrl(String hlsUrl) {
        this.hlsUrl = hlsUrl;
    }

    /**
     * @return The durationVideo
     */
    public Integer getDurationVideo() {
        return durationVideo;
    }

    /**
     * @param durationVideo The durationVideo
     */
    public void setDurationVideo(Integer durationVideo) {
        this.durationVideo = durationVideo;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
