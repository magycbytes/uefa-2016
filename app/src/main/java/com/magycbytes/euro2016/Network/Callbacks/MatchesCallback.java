package com.magycbytes.euro2016.Network.Callbacks;

import android.util.Log;

import com.magycbytes.euro2016.Models.TodayMatches.MatchesResponse;
import com.magycbytes.euro2016.Screens.TodayMatches.MatchesPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandru on 6/17/16.
 */
public class MatchesCallback implements Callback<MatchesResponse> {

    private final MatchesPresenter mPresenter;

    public MatchesCallback(MatchesPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResponse(Call<MatchesResponse> call, Response<MatchesResponse> response) {
        mPresenter.hideProgress();

        if (response.code() != 200 || response.body() == null) {
            mPresenter.showErrorMessageToUser();
            return;
        }

        MatchesResponse body = response.body();
        mPresenter.showDownloadedMatches(body.getMatchInfoItems());
    }

    @Override
    public void onFailure(Call<MatchesResponse> call, Throwable t) {
        mPresenter.hideProgress();
        Log.e(this.toString(), t.getMessage());
        mPresenter.showErrorMessageToUser();
    }
}
