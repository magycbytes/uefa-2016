package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Scorers {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private List<HomeGoal> homeGoals = new ArrayList<>();
    private List<AwayGoal> awayGoals = new ArrayList<>();

    /**
     * @return The homeGoals
     */
    public List<HomeGoal> getHomeGoals() {
        return homeGoals;
    }

    /**
     * @param homeGoals The homeGoals
     */
    public void setHomeGoals(List<HomeGoal> homeGoals) {
        this.homeGoals = homeGoals;
    }

    /**
     * @return The awayGoals
     */
    public List<AwayGoal> getAwayGoals() {
        return awayGoals;
    }

    /**
     * @param awayGoals The awayGoals
     */
    public void setAwayGoals(List<AwayGoal> awayGoals) {
        this.awayGoals = awayGoals;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
