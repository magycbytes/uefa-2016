package com.magycbytes.euro2016;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * Created by alexandru on 6/15/16.
 */
class MainActivityView {

    private final Toolbar mToolbar;
    private final NavigationView mNavigationView;

    @SuppressWarnings("ConstantConditions")
    public MainActivityView(View rootView, Activity activity) {
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity) activity).setSupportActionBar(mToolbar);
        ((AppCompatActivity) activity).getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawer, mToolbar, R.string.open, R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) rootView.findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) activity);
    }

    public int getCurrentScreenNumber() {
        for (int i = 0; i < mNavigationView.getMenu().size(); ++i) {
            if (mNavigationView.getMenu().getItem(i).isChecked()) {
                return i;
            }
        }
        return -1;
    }

    public void checkItem(int position) {
        mNavigationView.getMenu().getItem(position).setChecked(true);
    }

    public void setToolbarTitle(String title) {
        mToolbar.setTitle(title);
    }
}
