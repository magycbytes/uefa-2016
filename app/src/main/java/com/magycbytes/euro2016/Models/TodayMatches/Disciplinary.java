package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Disciplinary {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private List<DisciplinaryHome> disciplinaryHome = new ArrayList<>();
    private List<DisciplinaryAway> disciplinaryAway = new ArrayList<>();

    /**
     * @return The disciplinaryHome
     */
    public List<DisciplinaryHome> getDisciplinaryHome() {
        return disciplinaryHome;
    }

    /**
     * @param disciplinaryHome The disciplinaryHome
     */
    public void setDisciplinaryHome(List<DisciplinaryHome> disciplinaryHome) {
        this.disciplinaryHome = disciplinaryHome;
    }

    /**
     * @return The disciplinaryAway
     */
    public List<DisciplinaryAway> getDisciplinaryAway() {
        return disciplinaryAway;
    }

    /**
     * @param disciplinaryAway The disciplinaryAway
     */
    public void setDisciplinaryAway(List<DisciplinaryAway> disciplinaryAway) {
        this.disciplinaryAway = disciplinaryAway;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
