package com.magycbytes.euro2016.Screens.TodayMatches.Helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.euro2016.Models.TodayMatches.MatchInfoItem;
import com.magycbytes.euro2016.R;

import java.util.List;

/**
 * Created by alexandru on 6/17/16.
 */
public class MatchesAdapter extends RecyclerView.Adapter<MatchHolder> {

    private final List<MatchInfoItem> mMatchInfos;
    private Context mContext;

    public MatchesAdapter(List<MatchInfoItem> matchInfos) {
        mMatchInfos = matchInfos;
    }

    @Override
    public MatchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_match,
                parent, false);

        return new MatchHolder(view);
    }

    @Override
    public void onBindViewHolder(MatchHolder holder, int position) {
        holder.loadFlags(mContext,
                mMatchInfos.get(position).getHomeTeamLogo().getLogo70(),
                mMatchInfos.get(position).getAwayTeamLogo().getLogo70());

        holder.showTeamNames(mMatchInfos.get(position).getHomeTeamName(),
                mMatchInfos.get(position).getAwayTeamName());
        holder.showGroupName(mMatchInfos.get(position).getGroupName());

        holder.showGoals(
                mMatchInfos.get(position).getResults().getHomeGoals(),
                mMatchInfos.get(position).getResults().getAwayGoals(),
                mMatchInfos.get(position).getDateTime()
        );

        holder.showCurrentMinute(
                mMatchInfos.get(position).getStatus(),
                mMatchInfos.get(position).getMinute(),
                mMatchInfos.get(position).getMinuteextra(),
                mMatchInfos.get(position).getMatchPhaseDetailed()
        );
    }

    @Override
    public int getItemCount() {
        return mMatchInfos.size();
    }
}
