package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.HashMap;
import java.util.Map;


public class WeatherConditions {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer humidity;
    private Integer temperature;
    private String weatherCondition;
    private String weatherConditionCode;
    private String pitchCondition;
    private String pitchConditionCode;
    private Integer windSpeed;

    /**
     * @return The humidity
     */
    public Integer getHumidity() {
        return humidity;
    }

    /**
     * @param humidity The humidity
     */
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    /**
     * @return The temperature
     */
    public Integer getTemperature() {
        return temperature;
    }

    /**
     * @param temperature The temperature
     */
    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    /**
     * @return The weatherCondition
     */
    public String getWeatherCondition() {
        return weatherCondition;
    }

    /**
     * @param weatherCondition The weatherCondition
     */
    public void setWeatherCondition(String weatherCondition) {
        this.weatherCondition = weatherCondition;
    }

    /**
     * @return The weatherConditionCode
     */
    public String getWeatherConditionCode() {
        return weatherConditionCode;
    }

    /**
     * @param weatherConditionCode The weatherConditionCode
     */
    public void setWeatherConditionCode(String weatherConditionCode) {
        this.weatherConditionCode = weatherConditionCode;
    }

    /**
     * @return The pitchCondition
     */
    public String getPitchCondition() {
        return pitchCondition;
    }

    /**
     * @param pitchCondition The pitchCondition
     */
    public void setPitchCondition(String pitchCondition) {
        this.pitchCondition = pitchCondition;
    }

    /**
     * @return The pitchConditionCode
     */
    public String getPitchConditionCode() {
        return pitchConditionCode;
    }

    /**
     * @param pitchConditionCode The pitchConditionCode
     */
    public void setPitchConditionCode(String pitchConditionCode) {
        this.pitchConditionCode = pitchConditionCode;
    }

    /**
     * @return The windSpeed
     */
    public Integer getWindSpeed() {
        return windSpeed;
    }

    /**
     * @param windSpeed The windSpeed
     */
    public void setWindSpeed(Integer windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
