package com.magycbytes.euro2016.Network.Callbacks;

import android.util.Log;

import com.magycbytes.euro2016.Models.Groups.Group;
import com.magycbytes.euro2016.Models.Groups.GroupResponse;
import com.magycbytes.euro2016.Screens.Standings.StandingsPresenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandru on 6/16/16.
 */
public class StandingsCallback implements Callback<GroupResponse> {

    private final StandingsPresenter mPresenter;

    public StandingsCallback(StandingsPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResponse(Call<GroupResponse> call, Response<GroupResponse> response) {
        mPresenter.hideProgress();

        if (response.code() != 200 || response.body() == null) {
            mPresenter.showErrorMessageToUser();
            return;
        }

        List<Group> groups = response.body().getGroups();
        mPresenter.showDownloadedGroups(groups);
    }

    @Override
    public void onFailure(Call<GroupResponse> call, Throwable t) {
        mPresenter.hideProgress();
        Log.e(this.toString(), t.getMessage());
        mPresenter.showErrorMessageToUser();
    }
}
