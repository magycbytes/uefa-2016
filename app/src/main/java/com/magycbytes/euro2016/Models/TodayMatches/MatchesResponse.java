package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MatchesResponse {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private List<MatchInfoItem> matchInfoItems = new ArrayList<>();
    private Pagination pagination;
    private Meta meta;
    private Integer mediaTypeId;

    /**
     * @return The matchInfoItems
     */
    public List<MatchInfoItem> getMatchInfoItems() {
        return matchInfoItems;
    }

    /**
     * @param matchInfoItems The matchInfoItems
     */
    public void setMatchInfoItems(List<MatchInfoItem> matchInfoItems) {
        this.matchInfoItems = matchInfoItems;
    }

    /**
     * @return The pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * @param pagination The pagination
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    /**
     * @return The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * @return The mediaTypeId
     */
    public Integer getMediaTypeId() {
        return mediaTypeId;
    }

    /**
     * @param mediaTypeId The mediaTypeId
     */
    public void setMediaTypeId(Integer mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
