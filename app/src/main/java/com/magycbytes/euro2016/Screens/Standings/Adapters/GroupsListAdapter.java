package com.magycbytes.euro2016.Screens.Standings.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.magycbytes.euro2016.Models.Groups.Standing;
import com.magycbytes.euro2016.R;

import java.util.Map;

/**
 * Created by alexandru on 6/14/16.
 */
public class GroupsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private final Map<Integer, Object> mGroups;

    public GroupsListAdapter(Map<Integer, Object> groups) {
        mGroups = groups;
    }

    @Override
    public int getItemViewType(int position) {
        if (mGroups.get(position) instanceof Standing) {
            return TYPE_ITEM;
        } else {
            return TYPE_SEPARATOR;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View resultView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_team,
                    parent, false);
            return new ViewHolderTeam(resultView);
        }
        View resultView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_group, parent,
                false);
        return new ViewHolderGroup(resultView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderTeam) {
            ViewHolderTeam viewHolderTeam = (ViewHolderTeam) holder;

            Standing standing = (Standing) mGroups.get(position);

            viewHolderTeam.mTeamName.setText(standing.getTeam().getOfficialName());
            viewHolderTeam.mGamesPlayed.setText(String.valueOf(standing.getPlayed()));
            viewHolderTeam.mGamesWon.setText(String.valueOf(standing.getWon()));
            viewHolderTeam.mGamesDraw.setText(String.valueOf(standing.getDrawn()));
            viewHolderTeam.mGamesLost.setText(String.valueOf(standing.getLost()));
            viewHolderTeam.mForGames.setText(String.valueOf(standing.getGoalsFor()));
            viewHolderTeam.mAgainstGames.setText(String.valueOf(standing.getGoalsAgainst()));
            viewHolderTeam.mGoalDifferences.setText(String.valueOf(standing.getGoalDifference()));
            viewHolderTeam.mPoints.setText(String.valueOf(standing.getPoints()));

        } else {
            ViewHolderGroup viewHolderGroup = (ViewHolderGroup) holder;
            viewHolderGroup.mGroupName.setText((String) mGroups.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mGroups.size();
    }

    public static class ViewHolderGroup extends RecyclerView.ViewHolder {

        final TextView mGroupName;

        public ViewHolderGroup(View itemView) {
            super(itemView);

            mGroupName = (TextView) itemView.findViewById(R.id.groupName);
        }
    }

    public static class ViewHolderTeam extends RecyclerView.ViewHolder {

        final TextView mTeamName;
        final TextView mGamesPlayed;
        final TextView mGamesWon;
        final TextView mGamesDraw;
        final TextView mGamesLost;
        final TextView mForGames;
        final TextView mAgainstGames;
        final TextView mGoalDifferences;
        final TextView mPoints;

        public ViewHolderTeam(View itemView) {
            super(itemView);

            mTeamName = (TextView) itemView.findViewById(R.id.teamName);
            mGamesPlayed = (TextView) itemView.findViewById(R.id.playedGames);
            mGamesWon = (TextView) itemView.findViewById(R.id.gamesWin);
            mGamesDraw = (TextView) itemView.findViewById(R.id.gamesDraw);
            mGamesLost = (TextView) itemView.findViewById(R.id.gameLose);
            mForGames = (TextView) itemView.findViewById(R.id.forGames);
            mAgainstGames = (TextView) itemView.findViewById(R.id.againstGames);
            mGoalDifferences = (TextView) itemView.findViewById(R.id.goalDifferences);
            mPoints = (TextView) itemView.findViewById(R.id.points);
        }
    }
}
