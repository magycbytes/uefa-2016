package com.magycbytes.euro2016.Models.Groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeprecationWarning {

    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("deadline")
    @Expose
    private Object deadline;

    /**
     * @return The message
     */
    public Object getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(Object message) {
        this.message = message;
    }

    /**
     * @return The deadline
     */
    public Object getDeadline() {
        return deadline;
    }

    /**
     * @param deadline The deadline
     */
    public void setDeadline(Object deadline) {
        this.deadline = deadline;
    }

}
