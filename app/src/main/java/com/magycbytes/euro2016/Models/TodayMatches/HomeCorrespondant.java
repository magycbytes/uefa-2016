package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.HashMap;
import java.util.Map;


public class HomeCorrespondant {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String tumblrusername;
    private String displayname;
    private String tumblremail;
    private String photofilename;

    /**
     * @return The tumblrusername
     */
    public String getTumblrusername() {
        return tumblrusername;
    }

    /**
     * @param tumblrusername The tumblrusername
     */
    public void setTumblrusername(String tumblrusername) {
        this.tumblrusername = tumblrusername;
    }

    /**
     * @return The displayname
     */
    public String getDisplayname() {
        return displayname;
    }

    /**
     * @param displayname The displayname
     */
    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    /**
     * @return The tumblremail
     */
    public String getTumblremail() {
        return tumblremail;
    }

    /**
     * @param tumblremail The tumblremail
     */
    public void setTumblremail(String tumblremail) {
        this.tumblremail = tumblremail;
    }

    /**
     * @return The photofilename
     */
    public String getPhotofilename() {
        return photofilename;
    }

    /**
     * @param photofilename The photofilename
     */
    public void setPhotofilename(String photofilename) {
        this.photofilename = photofilename;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
