package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.HashMap;
import java.util.Map;


public class EditorialHighLight {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String hdsUrl;
    private String hlsUrl;
    private Integer id;

    /**
     * @return The hdsUrl
     */
    public String getHdsUrl() {
        return hdsUrl;
    }

    /**
     * @param hdsUrl The hdsUrl
     */
    public void setHdsUrl(String hdsUrl) {
        this.hdsUrl = hdsUrl;
    }

    /**
     * @return The hlsUrl
     */
    public String getHlsUrl() {
        return hlsUrl;
    }

    /**
     * @param hlsUrl The hlsUrl
     */
    public void setHlsUrl(String hlsUrl) {
        this.hlsUrl = hlsUrl;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
