package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Medium {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer mediaTypeId;
    private List<ImageUrl> imageUrls = new ArrayList<>();
    private String title;
    private String headline;
    private String copyright;
    private String hdsUrl;
    private String hlsUrl;

    /**
     * @return The mediaTypeId
     */
    public Integer getMediaTypeId() {
        return mediaTypeId;
    }

    /**
     * @param mediaTypeId The mediaTypeId
     */
    public void setMediaTypeId(Integer mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    /**
     * @return The imageUrls
     */
    public List<ImageUrl> getImageUrls() {
        return imageUrls;
    }

    /**
     * @param imageUrls The imageUrls
     */
    public void setImageUrls(List<ImageUrl> imageUrls) {
        this.imageUrls = imageUrls;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The headline
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * @param headline The headline
     */
    public void setHeadline(String headline) {
        this.headline = headline;
    }

    /**
     * @return The copyright
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * @param copyright The copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * @return The hdsUrl
     */
    public String getHdsUrl() {
        return hdsUrl;
    }

    /**
     * @param hdsUrl The hdsUrl
     */
    public void setHdsUrl(String hdsUrl) {
        this.hdsUrl = hdsUrl;
    }

    /**
     * @return The hlsUrl
     */
    public String getHlsUrl() {
        return hlsUrl;
    }

    /**
     * @param hlsUrl The hlsUrl
     */
    public void setHlsUrl(String hlsUrl) {
        this.hlsUrl = hlsUrl;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
