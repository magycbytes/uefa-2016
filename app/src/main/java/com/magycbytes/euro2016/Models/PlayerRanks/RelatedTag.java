package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RelatedTag {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String tagType;
    private List<Integer> tags = new ArrayList<>();

    /**
     * @return The tagType
     */
    public String getTagType() {
        return tagType;
    }

    /**
     * @param tagType The tagType
     */
    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    /**
     * @return The tags
     */
    public List<Integer> getTags() {
        return tags;
    }

    /**
     * @param tags The tags
     */
    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
