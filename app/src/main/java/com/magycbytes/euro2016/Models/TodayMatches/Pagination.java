package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.HashMap;
import java.util.Map;


public class Pagination {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String nextUrl;
    private String previousUrl;
    private Integer maxItems;

    /**
     * @return The nextUrl
     */
    public String getNextUrl() {
        return nextUrl;
    }

    /**
     * @param nextUrl The nextUrl
     */
    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    /**
     * @return The previousUrl
     */
    public String getPreviousUrl() {
        return previousUrl;
    }

    /**
     * @param previousUrl The previousUrl
     */
    public void setPreviousUrl(String previousUrl) {
        this.previousUrl = previousUrl;
    }

    /**
     * @return The maxItems
     */
    public Integer getMaxItems() {
        return maxItems;
    }

    /**
     * @param maxItems The maxItems
     */
    public void setMaxItems(Integer maxItems) {
        this.maxItems = maxItems;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
