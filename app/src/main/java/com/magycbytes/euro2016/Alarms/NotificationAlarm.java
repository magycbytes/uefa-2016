package com.magycbytes.euro2016.Alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.magycbytes.euro2016.Services.TimeToMatchNotificationService;

import java.util.Date;
import java.util.Random;

/**
 * Created by alexandru on 6/21/16.
 */

public class NotificationAlarm {

    private static final String TAG = "NotificationAlarm";

    public static void setAlarm(Context context, long afterTimeMilliseconds, String message,
                                String title) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        int serviceId = new Random().nextInt();
        Intent intent = TimeToMatchNotificationService.getIntent(context, title, message);
        PendingIntent pendingIntent = PendingIntent.getService(context, serviceId, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        long alarmTime = System.currentTimeMillis() + afterTimeMilliseconds;

        Log.i(TAG, new Date(alarmTime).toString() + ", " + serviceId);
        Log.i(TAG, title + ", " + message);

        alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);
    }
}
