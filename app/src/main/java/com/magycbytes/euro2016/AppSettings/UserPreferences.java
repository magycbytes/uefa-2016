package com.magycbytes.euro2016.AppSettings;

import android.content.Context;
import android.content.SharedPreferences;

import com.magycbytes.euro2016.MainActivityPresenter;

/**
 * Created by alexandru on 6/14/16.
 */
public class UserPreferences {

    public static final int TABLE_VIEW = 0;
    public static final int CARD_VIEW = 1;
    public static final String VIEW_LAYOUT = "viewLayout";
    private static final String USER_PREFERENCES = "userPreferences";
    private static final String PHASE_TOURNAMENT_STANDINGS = "phaseTournamentStandings";
    private static final int QUALIFICATION_PHASE = 1;
    private static final String PHASE_TOURNAMENT_ALL_MATCHES = "phaseTournamentAllMatches";
    private static final String LAST_USER_SCREEN = "lastUserScreen";

    public static int getLastUserScreen(Context context) {
        return getPreference(context, LAST_USER_SCREEN, MainActivityPresenter.STANDINGS_SCREEN);
    }

    public static void saveLastUserScreen(Context context, int value) {
        writePreference(context, LAST_USER_SCREEN, value);
    }

    public static int getPhaseTournamentAllMatches(Context context) {
        return getPreference(context, PHASE_TOURNAMENT_ALL_MATCHES, QUALIFICATION_PHASE);
    }

    public static void savePhaseTournamentAllMatches(Context context, int value) {
        writePreference(context, PHASE_TOURNAMENT_ALL_MATCHES, value);
    }

    public static int getViewPreference(Context context) {
        return getPreference(context, VIEW_LAYOUT, CARD_VIEW);
    }

    public static void saveViewPreference(Context context, int value) {
        writePreference(context, VIEW_LAYOUT, value);
    }

    public static void savePhaseTournamentStandings(Context context, int value) {
        writePreference(context, PHASE_TOURNAMENT_STANDINGS, value);
    }

    public static int getPhaseTournamentStandings(Context context) {
        return getPreference(context, PHASE_TOURNAMENT_STANDINGS, QUALIFICATION_PHASE);
    }

    public static void registerListener(Context context,
                                        SharedPreferences.OnSharedPreferenceChangeListener
                                                listener) {
        context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(listener);
    }

    private static int getPreference(Context context, String key, int defaultValue) {
        return context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE).getInt(key,
                defaultValue);
    }

    private static void writePreference(Context context, String key, int value) {
        context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE).edit().putInt
                (key, value).apply();
    }
}
