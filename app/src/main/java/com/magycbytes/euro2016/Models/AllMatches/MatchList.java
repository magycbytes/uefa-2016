
package com.magycbytes.euro2016.Models.AllMatches;

import java.util.HashMap;
import java.util.Map;


public class MatchList {

    private String competitionName;
    private Integer idMatch;
    private Integer idCup;
    private Integer idSeason;
    private Integer status;
    private String statusDescr;
    private String date;
    private String time;
    private String dateTime;
    private Integer minute;
    private Integer minuteextra;
    private Integer idHomeTeam;
    private String homeTeamName;
    private String homeTeamShortName;
    private String homeCountryCode;
    private Integer idAwayTeam;
    private String awayTeamName;
    private String awayTeamShortName;
    private String awayCountryCode;
    private Integer typeTeamHome;
    private Integer typeTeamAway;
    private Integer idReferee;
    private Integer matchDay;
    private Integer matchNumber;
    private Integer session;
    private Integer idRound;
    private String roundName;
    private String roundNameFull;
    private Integer matchPhase;
    private String matchPhaseDetailed;
    private String matchPhaseDetailedCode;
    private Integer idGroup;
    private String groupName;
    private Integer idStadium;
    private String stadiumName;
    private Integer uefaCapacity;
    private String constructionDate;
    private String stadiumThumb;
    private Integer idVenue;
    private String venueName;
    private String countryCode;
    private String lineup;
    private Results results;
    private HomeTeamLogo homeTeamLogo;
    private AwayTeamLogo awayTeamLogo;
    private Integer tournamentPhase;
    private String tournamentPhaseName;
    private LastComment lastComment;
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The competitionName
     */
    public String getCompetitionName() {
        return competitionName;
    }

    /**
     * 
     * @param competitionName
     *     The competitionName
     */
    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    /**
     * 
     * @return
     *     The idMatch
     */
    public Integer getIdMatch() {
        return idMatch;
    }

    /**
     * 
     * @param idMatch
     *     The idMatch
     */
    public void setIdMatch(Integer idMatch) {
        this.idMatch = idMatch;
    }

    /**
     * 
     * @return
     *     The idCup
     */
    public Integer getIdCup() {
        return idCup;
    }

    /**
     * 
     * @param idCup
     *     The idCup
     */
    public void setIdCup(Integer idCup) {
        this.idCup = idCup;
    }

    /**
     * 
     * @return
     *     The idSeason
     */
    public Integer getIdSeason() {
        return idSeason;
    }

    /**
     * 
     * @param idSeason
     *     The idSeason
     */
    public void setIdSeason(Integer idSeason) {
        this.idSeason = idSeason;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The statusDescr
     */
    public String getStatusDescr() {
        return statusDescr;
    }

    /**
     * 
     * @param statusDescr
     *     The statusDescr
     */
    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The time
     */
    public String getTime() {
        return time;
    }

    /**
     * 
     * @param time
     *     The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 
     * @return
     *     The dateTime
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * 
     * @param dateTime
     *     The dateTime
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * 
     * @return
     *     The minute
     */
    public Integer getMinute() {
        return minute;
    }

    /**
     * 
     * @param minute
     *     The minute
     */
    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    /**
     * 
     * @return
     *     The minuteextra
     */
    public Integer getMinuteextra() {
        return minuteextra;
    }

    /**
     * 
     * @param minuteextra
     *     The minuteextra
     */
    public void setMinuteextra(Integer minuteextra) {
        this.minuteextra = minuteextra;
    }

    /**
     * 
     * @return
     *     The idHomeTeam
     */
    public Integer getIdHomeTeam() {
        return idHomeTeam;
    }

    /**
     * 
     * @param idHomeTeam
     *     The idHomeTeam
     */
    public void setIdHomeTeam(Integer idHomeTeam) {
        this.idHomeTeam = idHomeTeam;
    }

    /**
     * 
     * @return
     *     The homeTeamName
     */
    public String getHomeTeamName() {
        return homeTeamName;
    }

    /**
     * 
     * @param homeTeamName
     *     The homeTeamName
     */
    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    /**
     * 
     * @return
     *     The homeTeamShortName
     */
    public String getHomeTeamShortName() {
        return homeTeamShortName;
    }

    /**
     * 
     * @param homeTeamShortName
     *     The homeTeamShortName
     */
    public void setHomeTeamShortName(String homeTeamShortName) {
        this.homeTeamShortName = homeTeamShortName;
    }

    /**
     * 
     * @return
     *     The homeCountryCode
     */
    public String getHomeCountryCode() {
        return homeCountryCode;
    }

    /**
     * 
     * @param homeCountryCode
     *     The homeCountryCode
     */
    public void setHomeCountryCode(String homeCountryCode) {
        this.homeCountryCode = homeCountryCode;
    }

    /**
     * 
     * @return
     *     The idAwayTeam
     */
    public Integer getIdAwayTeam() {
        return idAwayTeam;
    }

    /**
     * 
     * @param idAwayTeam
     *     The idAwayTeam
     */
    public void setIdAwayTeam(Integer idAwayTeam) {
        this.idAwayTeam = idAwayTeam;
    }

    /**
     * 
     * @return
     *     The awayTeamName
     */
    public String getAwayTeamName() {
        return awayTeamName;
    }

    /**
     * 
     * @param awayTeamName
     *     The awayTeamName
     */
    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    /**
     * 
     * @return
     *     The awayTeamShortName
     */
    public String getAwayTeamShortName() {
        return awayTeamShortName;
    }

    /**
     * 
     * @param awayTeamShortName
     *     The awayTeamShortName
     */
    public void setAwayTeamShortName(String awayTeamShortName) {
        this.awayTeamShortName = awayTeamShortName;
    }

    /**
     * 
     * @return
     *     The awayCountryCode
     */
    public String getAwayCountryCode() {
        return awayCountryCode;
    }

    /**
     * 
     * @param awayCountryCode
     *     The awayCountryCode
     */
    public void setAwayCountryCode(String awayCountryCode) {
        this.awayCountryCode = awayCountryCode;
    }

    /**
     * 
     * @return
     *     The typeTeamHome
     */
    public Integer getTypeTeamHome() {
        return typeTeamHome;
    }

    /**
     * 
     * @param typeTeamHome
     *     The typeTeamHome
     */
    public void setTypeTeamHome(Integer typeTeamHome) {
        this.typeTeamHome = typeTeamHome;
    }

    /**
     * 
     * @return
     *     The typeTeamAway
     */
    public Integer getTypeTeamAway() {
        return typeTeamAway;
    }

    /**
     * 
     * @param typeTeamAway
     *     The typeTeamAway
     */
    public void setTypeTeamAway(Integer typeTeamAway) {
        this.typeTeamAway = typeTeamAway;
    }

    /**
     * 
     * @return
     *     The idReferee
     */
    public Integer getIdReferee() {
        return idReferee;
    }

    /**
     * 
     * @param idReferee
     *     The idReferee
     */
    public void setIdReferee(Integer idReferee) {
        this.idReferee = idReferee;
    }

    /**
     * 
     * @return
     *     The matchDay
     */
    public Integer getMatchDay() {
        return matchDay;
    }

    /**
     * 
     * @param matchDay
     *     The matchDay
     */
    public void setMatchDay(Integer matchDay) {
        this.matchDay = matchDay;
    }

    /**
     * 
     * @return
     *     The matchNumber
     */
    public Integer getMatchNumber() {
        return matchNumber;
    }

    /**
     * 
     * @param matchNumber
     *     The matchNumber
     */
    public void setMatchNumber(Integer matchNumber) {
        this.matchNumber = matchNumber;
    }

    /**
     * 
     * @return
     *     The session
     */
    public Integer getSession() {
        return session;
    }

    /**
     * 
     * @param session
     *     The session
     */
    public void setSession(Integer session) {
        this.session = session;
    }

    /**
     * 
     * @return
     *     The idRound
     */
    public Integer getIdRound() {
        return idRound;
    }

    /**
     * 
     * @param idRound
     *     The idRound
     */
    public void setIdRound(Integer idRound) {
        this.idRound = idRound;
    }

    /**
     * 
     * @return
     *     The roundName
     */
    public String getRoundName() {
        return roundName;
    }

    /**
     * 
     * @param roundName
     *     The roundName
     */
    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }

    /**
     * 
     * @return
     *     The roundNameFull
     */
    public String getRoundNameFull() {
        return roundNameFull;
    }

    /**
     * 
     * @param roundNameFull
     *     The roundNameFull
     */
    public void setRoundNameFull(String roundNameFull) {
        this.roundNameFull = roundNameFull;
    }

    /**
     * 
     * @return
     *     The matchPhase
     */
    public Integer getMatchPhase() {
        return matchPhase;
    }

    /**
     * 
     * @param matchPhase
     *     The matchPhase
     */
    public void setMatchPhase(Integer matchPhase) {
        this.matchPhase = matchPhase;
    }

    /**
     * 
     * @return
     *     The matchPhaseDetailed
     */
    public String getMatchPhaseDetailed() {
        return matchPhaseDetailed;
    }

    /**
     * 
     * @param matchPhaseDetailed
     *     The matchPhaseDetailed
     */
    public void setMatchPhaseDetailed(String matchPhaseDetailed) {
        this.matchPhaseDetailed = matchPhaseDetailed;
    }

    /**
     * 
     * @return
     *     The matchPhaseDetailedCode
     */
    public String getMatchPhaseDetailedCode() {
        return matchPhaseDetailedCode;
    }

    /**
     * 
     * @param matchPhaseDetailedCode
     *     The matchPhaseDetailedCode
     */
    public void setMatchPhaseDetailedCode(String matchPhaseDetailedCode) {
        this.matchPhaseDetailedCode = matchPhaseDetailedCode;
    }

    /**
     * 
     * @return
     *     The idGroup
     */
    public Integer getIdGroup() {
        return idGroup;
    }

    /**
     * 
     * @param idGroup
     *     The idGroup
     */
    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    /**
     * 
     * @return
     *     The groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * 
     * @param groupName
     *     The groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * 
     * @return
     *     The idStadium
     */
    public Integer getIdStadium() {
        return idStadium;
    }

    /**
     * 
     * @param idStadium
     *     The idStadium
     */
    public void setIdStadium(Integer idStadium) {
        this.idStadium = idStadium;
    }

    /**
     * 
     * @return
     *     The stadiumName
     */
    public String getStadiumName() {
        return stadiumName;
    }

    /**
     * 
     * @param stadiumName
     *     The stadiumName
     */
    public void setStadiumName(String stadiumName) {
        this.stadiumName = stadiumName;
    }

    /**
     * 
     * @return
     *     The uefaCapacity
     */
    public Integer getUefaCapacity() {
        return uefaCapacity;
    }

    /**
     * 
     * @param uefaCapacity
     *     The uefaCapacity
     */
    public void setUefaCapacity(Integer uefaCapacity) {
        this.uefaCapacity = uefaCapacity;
    }

    /**
     * 
     * @return
     *     The constructionDate
     */
    public String getConstructionDate() {
        return constructionDate;
    }

    /**
     * 
     * @param constructionDate
     *     The constructionDate
     */
    public void setConstructionDate(String constructionDate) {
        this.constructionDate = constructionDate;
    }

    /**
     * 
     * @return
     *     The stadiumThumb
     */
    public String getStadiumThumb() {
        return stadiumThumb;
    }

    /**
     * 
     * @param stadiumThumb
     *     The stadiumThumb
     */
    public void setStadiumThumb(String stadiumThumb) {
        this.stadiumThumb = stadiumThumb;
    }

    /**
     * 
     * @return
     *     The idVenue
     */
    public Integer getIdVenue() {
        return idVenue;
    }

    /**
     * 
     * @param idVenue
     *     The idVenue
     */
    public void setIdVenue(Integer idVenue) {
        this.idVenue = idVenue;
    }

    /**
     * 
     * @return
     *     The venueName
     */
    public String getVenueName() {
        return venueName;
    }

    /**
     * 
     * @param venueName
     *     The venueName
     */
    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    /**
     * 
     * @return
     *     The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * 
     * @param countryCode
     *     The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * 
     * @return
     *     The lineup
     */
    public String getLineup() {
        return lineup;
    }

    /**
     * 
     * @param lineup
     *     The lineup
     */
    public void setLineup(String lineup) {
        this.lineup = lineup;
    }

    /**
     * 
     * @return
     *     The results
     */
    public Results getResults() {
        return results;
    }

    /**
     * 
     * @param results
     *     The results
     */
    public void setResults(Results results) {
        this.results = results;
    }

    /**
     * 
     * @return
     *     The homeTeamLogo
     */
    public HomeTeamLogo getHomeTeamLogo() {
        return homeTeamLogo;
    }

    /**
     * 
     * @param homeTeamLogo
     *     The homeTeamLogo
     */
    public void setHomeTeamLogo(HomeTeamLogo homeTeamLogo) {
        this.homeTeamLogo = homeTeamLogo;
    }

    /**
     * 
     * @return
     *     The awayTeamLogo
     */
    public AwayTeamLogo getAwayTeamLogo() {
        return awayTeamLogo;
    }

    /**
     * 
     * @param awayTeamLogo
     *     The awayTeamLogo
     */
    public void setAwayTeamLogo(AwayTeamLogo awayTeamLogo) {
        this.awayTeamLogo = awayTeamLogo;
    }

    /**
     * 
     * @return
     *     The tournamentPhase
     */
    public Integer getTournamentPhase() {
        return tournamentPhase;
    }

    /**
     * 
     * @param tournamentPhase
     *     The tournamentPhase
     */
    public void setTournamentPhase(Integer tournamentPhase) {
        this.tournamentPhase = tournamentPhase;
    }

    /**
     * 
     * @return
     *     The tournamentPhaseName
     */
    public String getTournamentPhaseName() {
        return tournamentPhaseName;
    }

    /**
     * 
     * @param tournamentPhaseName
     *     The tournamentPhaseName
     */
    public void setTournamentPhaseName(String tournamentPhaseName) {
        this.tournamentPhaseName = tournamentPhaseName;
    }

    /**
     * 
     * @return
     *     The lastComment
     */
    public LastComment getLastComment() {
        return lastComment;
    }

    /**
     * 
     * @param lastComment
     *     The lastComment
     */
    public void setLastComment(LastComment lastComment) {
        this.lastComment = lastComment;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
