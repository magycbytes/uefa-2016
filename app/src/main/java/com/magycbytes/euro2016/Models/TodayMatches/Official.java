package com.magycbytes.euro2016.Models.TodayMatches;

import java.util.HashMap;
import java.util.Map;


class Official {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer idPeople;
    private String countryCode;
    private String peopleWebName;
    private String peopleWebNameAlt;
    private String peopleMatchKind;
    private String peopleImage;
    private String peopleMatchKindTag;

    /**
     * @return The idPeople
     */
    public Integer getIdPeople() {
        return idPeople;
    }

    /**
     * @param idPeople The idPeople
     */
    public void setIdPeople(Integer idPeople) {
        this.idPeople = idPeople;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return The peopleWebName
     */
    public String getPeopleWebName() {
        return peopleWebName;
    }

    /**
     * @param peopleWebName The peopleWebName
     */
    public void setPeopleWebName(String peopleWebName) {
        this.peopleWebName = peopleWebName;
    }

    /**
     * @return The peopleWebNameAlt
     */
    public String getPeopleWebNameAlt() {
        return peopleWebNameAlt;
    }

    /**
     * @param peopleWebNameAlt The peopleWebNameAlt
     */
    public void setPeopleWebNameAlt(String peopleWebNameAlt) {
        this.peopleWebNameAlt = peopleWebNameAlt;
    }

    /**
     * @return The peopleMatchKind
     */
    public String getPeopleMatchKind() {
        return peopleMatchKind;
    }

    /**
     * @param peopleMatchKind The peopleMatchKind
     */
    public void setPeopleMatchKind(String peopleMatchKind) {
        this.peopleMatchKind = peopleMatchKind;
    }

    /**
     * @return The peopleImage
     */
    public String getPeopleImage() {
        return peopleImage;
    }

    /**
     * @param peopleImage The peopleImage
     */
    public void setPeopleImage(String peopleImage) {
        this.peopleImage = peopleImage;
    }

    /**
     * @return The peopleMatchKindTag
     */
    public String getPeopleMatchKindTag() {
        return peopleMatchKindTag;
    }

    /**
     * @param peopleMatchKindTag The peopleMatchKindTag
     */
    public void setPeopleMatchKindTag(String peopleMatchKindTag) {
        this.peopleMatchKindTag = peopleMatchKindTag;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
