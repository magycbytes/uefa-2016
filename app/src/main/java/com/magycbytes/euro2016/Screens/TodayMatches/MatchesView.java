package com.magycbytes.euro2016.Screens.TodayMatches;

import android.view.View;
import android.widget.TextView;

import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Screens.Helpers.ScreenView;

/**
 * Created by alexandru on 6/17/16.
 */
class MatchesView extends ScreenView{

    private final TextView mMatchDay;

    MatchesView(View rootView) {
        super(rootView);

        mMatchDay = (TextView) rootView.findViewById(R.id.matchDay);
    }

    void showMatchDay(String date) {
        mMatchDay.setText(date);
    }
}
