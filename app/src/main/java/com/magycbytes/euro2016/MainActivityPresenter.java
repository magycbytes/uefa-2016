package com.magycbytes.euro2016;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.magycbytes.euro2016.AppSettings.UserPreferences;
import com.magycbytes.euro2016.Screens.AllMatches.AllMatchesFragment;
import com.magycbytes.euro2016.Screens.TodayMatches.MatchesFragment;
import com.magycbytes.euro2016.Screens.PlayersRank.PlayersRankFragment;
import com.magycbytes.euro2016.Screens.Standings.StandingsFragment;

/**
 * Created by alexandru on 6/15/16.
 */
public class MainActivityPresenter {

    public static final int STANDINGS_SCREEN = 0;
    private static final int PLAYER_RANKS_SCREEN = 1;
    private static final int MATCHES_SCREEN = 2;
    private static final int ALL_MATCHES_SCREEN = 3;

    private static final int NO_FRAGMENT_TITLE = -1;

    private final MainActivityView mActivityView;
    private final AppCompatActivity mActivityCompat;
    private final StandingsLayoutSelectorView mLayoutSelectorView;
    private Fragment mCurrentFragment;


    MainActivityPresenter(
            MainActivityView activityView,
            AppCompatActivity activityCompat,
            StandingsLayoutSelectorView layoutSelectorView) {

        mActivityView = activityView;
        mActivityCompat = activityCompat;
        mLayoutSelectorView = layoutSelectorView;
    }

    void showScreen(int screenNumber) {
        switch (screenNumber) {
            case STANDINGS_SCREEN:
                showStandingsFragment();
                break;
            case PLAYER_RANKS_SCREEN:
                showPlayersRankFragment();
                break;
            case MATCHES_SCREEN:
                showTodayMatchesFragment();
                break;
            case ALL_MATCHES_SCREEN:
                showAllMatchesFragment();
                break;
            default:
                showStandingsFragment();
        }
    }

    void updateContentInFragment() {
        if (mCurrentFragment instanceof StandingsFragment) {
            StandingsFragment standingsFragment = (StandingsFragment) mCurrentFragment;
            standingsFragment.refresh();
        } else if (mCurrentFragment instanceof PlayersRankFragment) {
            PlayersRankFragment playersRankFragment = (PlayersRankFragment) mCurrentFragment;
            playersRankFragment.refresh();
        } else if (mCurrentFragment instanceof MatchesFragment) {
            MatchesFragment matchesFragment = (MatchesFragment) mCurrentFragment;
            matchesFragment.refresh();
        } else if (mCurrentFragment instanceof AllMatchesFragment) {
            AllMatchesFragment allMatchesFragment = (AllMatchesFragment) mCurrentFragment;
            allMatchesFragment.refresh();
        }
    }

    void showStandingsFragment() {
        StandingsFragment standingsFragment = new StandingsFragment();
        showNewFragment(standingsFragment, STANDINGS_SCREEN, NO_FRAGMENT_TITLE);
    }

    void showPlayersRankFragment() {
        PlayersRankFragment playersRankFragment = new PlayersRankFragment();
        showNewFragment(playersRankFragment, PLAYER_RANKS_SCREEN, R.string.players_top_uefa);
    }

    void showTodayMatchesFragment() {
        MatchesFragment matchesFragment = new MatchesFragment();
        showNewFragment(matchesFragment, MATCHES_SCREEN, R.string.matches_today);
    }

    void showAllMatchesFragment() {
        AllMatchesFragment fragment = new AllMatchesFragment();
        showNewFragment(fragment, ALL_MATCHES_SCREEN, R.string.show_all_matches);
    }

    int getSelectedScreenPosition() {
        return mActivityView.getCurrentScreenNumber();
    }

    private void showNewFragment(Fragment fragment, int fragmentNr, int titleId) {
        changeFragment(fragment);
        mActivityView.checkItem(fragmentNr);

        if (titleId == NO_FRAGMENT_TITLE) {
            mActivityView.setToolbarTitle(null);
            mLayoutSelectorView.showSpinner();
        } else {
            mLayoutSelectorView.hideSpinner();
            mActivityView.setToolbarTitle(mActivityCompat.getString(titleId));
        }
        UserPreferences.saveLastUserScreen(mActivityCompat, fragmentNr);
    }

    private void changeFragment(Fragment fragment) {
        mCurrentFragment = fragment;
        mActivityCompat
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }
}
