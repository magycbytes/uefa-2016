package com.magycbytes.euro2016.Screens.PlayersRank.helpers;

import android.content.Context;

import com.magycbytes.euro2016.R;

/**
 * Created by alexandru on 6/15/16.
 */
public class ColumnsCalculator {

    public static int getColumnsNumber(Context context) {
        int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
        int imageDimen = context.getResources().getDimensionPixelSize(R.dimen.player_image_width);

        return widthPixels / imageDimen;
    }
}
