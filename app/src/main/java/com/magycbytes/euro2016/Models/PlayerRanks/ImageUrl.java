package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.HashMap;
import java.util.Map;

class ImageUrl {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private Integer imageId;
    private String baseUrl;
    private String w1;
    private String w3;
    private String w6;
    private String s1;
    private String s3;
    private String s6;

    /**
     * @return The imageId
     */
    public Integer getImageId() {
        return imageId;
    }

    /**
     * @param imageId The imageId
     */
    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    /**
     * @return The baseUrl
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl The baseUrl
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @return The w1
     */
    public String getW1() {
        return w1;
    }

    /**
     * @param w1 The w1
     */
    public void setW1(String w1) {
        this.w1 = w1;
    }

    /**
     * @return The w3
     */
    public String getW3() {
        return w3;
    }

    /**
     * @param w3 The w3
     */
    public void setW3(String w3) {
        this.w3 = w3;
    }

    /**
     * @return The w6
     */
    public String getW6() {
        return w6;
    }

    /**
     * @param w6 The w6
     */
    public void setW6(String w6) {
        this.w6 = w6;
    }

    /**
     * @return The s1
     */
    public String getS1() {
        return s1;
    }

    /**
     * @param s1 The s1
     */
    public void setS1(String s1) {
        this.s1 = s1;
    }

    /**
     * @return The s3
     */
    public String getS3() {
        return s3;
    }

    /**
     * @param s3 The s3
     */
    public void setS3(String s3) {
        this.s3 = s3;
    }

    /**
     * @return The s6
     */
    public String getS6() {
        return s6;
    }

    /**
     * @param s6 The s6
     */
    public void setS6(String s6) {
        this.s6 = s6;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
