package com.magycbytes.euro2016;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.magycbytes.euro2016.Alarms.AlarmMatchesCheck;
import com.magycbytes.euro2016.AppSettings.UserPreferences;
import com.magycbytes.euro2016.Services.TodayMatchesNotificationService;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private static final String SELECTED_SCREEN = "selectedScreen";
    private MainActivityPresenter mMainActivityPresenter;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityView mainActivityView = new MainActivityView(findViewById(R.id.drawer_layout)
                , this);

        StandingsLayoutSelectorView layoutSelectorView = new StandingsLayoutSelectorView(findViewById(R.id.drawer_layout));
        mMainActivityPresenter = new MainActivityPresenter(mainActivityView, this, layoutSelectorView);

        int screenNumber = savedInstanceState != null
                ? savedInstanceState.getInt(SELECTED_SCREEN)
                : UserPreferences.getLastUserScreen(this);
        mMainActivityPresenter.showScreen(screenNumber);

        AlarmMatchesCheck.setAlarm(this, TodayMatchesNotificationService.getIntent(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_action:
                mMainActivityPresenter.updateContentInFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_standings:
                mMainActivityPresenter.showStandingsFragment();
                break;
            case R.id.nav_players_rank:
                mMainActivityPresenter.showPlayersRankFragment();
                break;
            case R.id.nav_matches_today:
                mMainActivityPresenter.showTodayMatchesFragment();
                break;
            case R.id.show_all_matches:
                mMainActivityPresenter.showAllMatchesFragment();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_SCREEN, mMainActivityPresenter.getSelectedScreenPosition());
        super.onSaveInstanceState(outState);
    }
}
