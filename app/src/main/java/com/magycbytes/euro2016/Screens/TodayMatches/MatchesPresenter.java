package com.magycbytes.euro2016.Screens.TodayMatches;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.magycbytes.euro2016.AppSettings.UserInterface;
import com.magycbytes.euro2016.Models.TodayMatches.MatchInfoItem;
import com.magycbytes.euro2016.Models.TodayMatches.MatchesResponse;
import com.magycbytes.euro2016.Network.Callbacks.MatchesCallback;
import com.magycbytes.euro2016.Network.ManagerApi;
import com.magycbytes.euro2016.R;
import com.magycbytes.euro2016.Screens.Helpers.ScreenPresenter;
import com.magycbytes.euro2016.Screens.TodayMatches.Helpers.MatchHolder;
import com.magycbytes.euro2016.Screens.TodayMatches.Helpers.MatchesAdapter;
import com.magycbytes.euro2016.Utils.DateUtils;

import java.text.ParseException;
import java.util.List;

import retrofit2.Call;

/**
 * Created by alexandru on 6/17/16.
 */
public class MatchesPresenter extends ScreenPresenter implements SwipeRefreshLayout.OnRefreshListener {


    private final MatchesView mView;
    private final Context mContext;

    MatchesPresenter(MatchesView view, Context context) {
        super(context, view);

        mView = view;
        mContext = context;

        view.setRefreshListener(this);
    }

    void startDownloading() {
        super.refresh();

        String language = UserInterface.getInterfaceLanguage(mContext);
        Call<MatchesResponse> call = ManagerApi.getApi(mContext).getTodayMatches(language);
        call.enqueue(new MatchesCallback(this));
    }

    public void showDownloadedMatches(List<MatchInfoItem> matches) {
        if (matches.size() > 0) {
            String dateTime = matches.get(0).getDateTime();
            try {
                dateTime = DateUtils.getFormattedDayFromString(dateTime,
                        MatchHolder.DATE_TIME_FORMAT);
            } catch (ParseException e) {
                Log.e(this.toString(), e.getMessage());
            }
            mView.showMatchDay(dateTime);
        } else {
            showMessageToUser(R.string.no_data_found);
        }

        MatchesAdapter matchesAdapter = new MatchesAdapter(matches);
        mView.setAdapter(matchesAdapter);
    }

    @Override
    public void onRefresh() {
        startDownloading();
    }
}
