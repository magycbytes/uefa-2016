package com.magycbytes.euro2016.Models.PlayerRanks;

import java.util.HashMap;
import java.util.Map;

public class Image {

    private final Map<String, Object> additionalProperties = new HashMap<>();
    private String square;
    private String cardSmall;
    private String cardMedium;
    private String cardXLarge;
    private String headerMedium;
    private String headerXLarge;
    private String placeholder;

    /**
     * @return The square
     */
    public String getSquare() {
        return square;
    }

    /**
     * @param square The square
     */
    public void setSquare(String square) {
        this.square = square;
    }

    /**
     * @return The cardSmall
     */
    public String getCardSmall() {
        return cardSmall;
    }

    /**
     * @param cardSmall The cardSmall
     */
    public void setCardSmall(String cardSmall) {
        this.cardSmall = cardSmall;
    }

    /**
     * @return The cardMedium
     */
    public String getCardMedium() {
        return cardMedium;
    }

    /**
     * @param cardMedium The cardMedium
     */
    public void setCardMedium(String cardMedium) {
        this.cardMedium = cardMedium;
    }

    /**
     * @return The cardXLarge
     */
    public String getCardXLarge() {
        return cardXLarge;
    }

    /**
     * @param cardXLarge The cardXLarge
     */
    public void setCardXLarge(String cardXLarge) {
        this.cardXLarge = cardXLarge;
    }

    /**
     * @return The headerMedium
     */
    public String getHeaderMedium() {
        return headerMedium;
    }

    /**
     * @param headerMedium The headerMedium
     */
    public void setHeaderMedium(String headerMedium) {
        this.headerMedium = headerMedium;
    }

    /**
     * @return The headerXLarge
     */
    public String getHeaderXLarge() {
        return headerXLarge;
    }

    /**
     * @param headerXLarge The headerXLarge
     */
    public void setHeaderXLarge(String headerXLarge) {
        this.headerXLarge = headerXLarge;
    }

    /**
     * @return The placeholder
     */
    public String getPlaceholder() {
        return placeholder;
    }

    /**
     * @param placeholder The placeholder
     */
    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
